# Carbon and Classic #

Advert posting site with Paypal payments. Developed in [Laravel](http://laravel.com) 4.2.

[Dan Walker](mailto:dwalker109@gmail.com)

# Some Notes

## Scheduled Tasks

Usus Indatus Dispatcher to manage tasks. This requires a single entry be added 
to `crond`, from which all other tasks will run. Make sure this entry is added
or no tasks will run.

## Adverts vs. Listings

These terms are used somewhat interchangeably. Model bindings seem to clash if
the binding is, for example, defined once for use in an /admin route and once
in a /member route. The solution seems to be to bind them with different names.
A little confusing but manageable.

## Activities vs Alerts

A decision was made late in development to rename "activities" to "alerts". This
has only been done on frontend labels and routes - the underlying models, controllers
etc. still refer to Activities.

## Currency

Only GBP pounds sterling is supported. Non UK PayPal accounts can be used but
all prices listed and generated are in sterling. Currency formatting is 
handled by `cartalyst/converter`.

# Additional Software #

Composer for PHP, see `./src/composer.json` and `./src/composer.lock`
Bower for frontend JS, see `./src/bower.json`
NodeJS for workflow JS, see `./src/package.json`
