<?php

use Carbon\Carbon;

class TransactionObserver
{

    /**
     * After saving a successful transaction, update the referenced advert with
     * purchased time to signal a completed purchase (if necessary)
     *
     * @param  Transaction $transaction
     */
    public function saved($transaction)
    {
        // Status must be complete, sender email must be set, no update if set already
        if ($transaction->paypal_status == Transaction::PP_TRANS_COMPLETE
                && $transaction->paypal_sender_email
                && !$transaction->advert->purchased_at) {
            $transaction->advert->fill(['purchased_at' => Carbon::now()])->save();
        }
    }
}
