<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ActivityFeedMaintenance extends ScheduledCommand {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dbmaintenance:activities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans out expired, read activities.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * When a command should run
     *
     * @param Scheduler $scheduler
     * @return \Indatus\Dispatcher\Scheduling\Schedulable
     */
    public function schedule(Schedulable $scheduler)
    {
        // Run every hour
        return $scheduler->hourly();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // Force delete any read activity notifications untouched for one month
        Activity::where('read', '=', true)
            ->where('updated_at', '<', \Carbon\Carbon::now()->subMonth())
            ->delete();
    }
}
