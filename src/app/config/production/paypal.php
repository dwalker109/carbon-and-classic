<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | PayPal
    |--------------------------------------------------------------------------
    |
    | Credentials for dealing with PayPal are specified here
    |
    */


    // Email address of PayPal account to receive commission
    'email' => '#',

    // Full PayPal Adaptive Payment endpoint URL, ready to append a payKey
    'auth_url' => 'https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=',

    // URL used to receive PayPal IPN messages
    'ipn_url' => 'http://#/ipn',

    // Duration allowed for a checkout to complete, as an XSD 'Duration' string.
    // See http://www.w3schools.com/schema/schema_dtypes_date.asp for details
    'pay_key_duration' => 'PT5M',

    // PayPal SDK global config
    'sdk_config' => [
        'mode' => 'sandbox',
        'acct1.UserName' => 'dwalker109-facilitator_api1.gmail.com',
        'acct1.Password' => '1381142482',
        'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AK6Xqr3Fs6ZFB4fVI524yHSoF5Hr',
        'acct1.AppId' => 'APP-80W284485P519543T',
/*        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 3,
        'log.LogEnabled' => true,*/
    ],

);
