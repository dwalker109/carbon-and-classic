<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "mail", "sendmail", "mailgun", "mandrill", "log"
    |
    */

    // Config for Mailcatcher (view mails on <host>:10800)

    'driver' => 'smtp',
    'host' => 'mailtrap.io',
    'port' => 2525,
    'from' => array(
        'address' => 'from@example.com',
        'name' => 'Example',
    ),
    'username' => '5ff075dba036b8',
    'password' => '22c555b03d2d42',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'pretend' => false,
);
