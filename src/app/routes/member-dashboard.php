<?php

/*
|--------------------------------------------------------------------------
| Member
|--------------------------------------------------------------------------
|
| Routes to deal with member dashboard pages
|
*/

Route::group(['prefix' => 'member', 'before' => 'entrust_user'], function() {

    Route::get('', ['as' => 'member.dashboard',
        function() {
            return View::make('member.dashboard.index');
        }
    ]);

    // Member profile and password management
    Route::get('profile/manage', ['as' => 'member.profile.manage',
        'uses' => 'Member\ProfileController@manage'
    ]);
    Route::post('profile/manage', ['as' => 'member.profile.doManage',
        'uses' => 'Member\ProfileController@doManage'
    ]);
    Route::get('password', ['as' => 'member.password',
        'uses' => 'Member\ProfileController@password'
    ]);
    Route::post('password', ['as' => 'member.doPassword',
        'uses' => 'Member\ProfileController@doPassword'
    ]);


    //////////////////////////////////////////////////////////////////////////
    // Adverts (AKA listings to avoid name collisions in the model binding) //
    //////////////////////////////////////////////////////////////////////////

    // 1) Specific routes without bindings
    Route::get('listings/all-active', [
        'as' => 'member.listings.all-active', 'uses' => 'Member\AdvertsController@allActiveSelling'
    ]);
    Route::get('listings/recently-sold', [
        'as' => 'member.listings.recently-sold', 'uses' => 'Member\AdvertsController@recentlySold'
    ]);

    
    // 2) Bind listings even if not editable for use in preview, display etc.
    Route::bind('listings_all', function($value) {
        return Advert::where('id', '=', $value)->ownedBySelf()->first();
    });

    // Define specific listings routes for use of this version
    Route::get('listings/{listings_all}/preview', [
        'as' => 'member.listings.preview', 'uses' => 'Member\AdvertsController@preview'
    ]);
    Route::get('listings/{listings_all}/summary', [
        'as' => 'member.listings.summary', 'uses' => 'Member\AdvertsController@summary'
    ]);
    Route::match(['POST', 'GET'], 'listings/{listings_all}/shipped', [
        'as' => 'member.listings.shipped', 'uses' => 'Member\AdvertsController@shipped'
    ]);
    Route::post('listings/{listings_all}/cancel-sale', [
        'as' => 'member.listings.cancel_sale', 'uses' => 'Member\AdvertsController@cancel'
    ]);

    // 3) Bind again for editable resources - most in a resource controller, plus some extras
    Route::bind('listings', function($value) {
        return Advert::where('id', '=', $value)->ownedBySelf()->editable()->first();
    });
    Route::resource('listings', 'Member\AdvertsController');
    Route::get('listings/{listings}/postage', [
        'as' => 'member.listings.postage', 'uses' => 'Member\AdvertsController@editPostage'
    ]);
    Route::post('listings/{listings}/postage', [
        'as' => 'member.listings.update_postage', 'uses' => 'Member\AdvertsController@updatePostage'
    ]);
    Route::get('listings/{listings}/interstitial', [
        'as' => 'member.listings.interstitial', 'uses' => 'Member\AdvertsController@interstitial',
    ]);
    Route::get('listings/{listings}/finished', [
        'as' => 'member.listings.finished', 'uses' => 'Member\AdvertsController@submitForApproval',
    ]);

    // Adverts galleries - uses advert binding defined above, and a new binding
    // for gallery pictures - existing ownership checks on the advert itself
    // will take care of ownership checks, no need for more here
    Route::group(['prefix' => 'listings/{listings}/gallery'], function () {

        Route::model('listings_gal_pic', 'GalleryPicture');

        Route::get('', [
            'as' => 'member.listings.gallery.index',
            'uses' => 'Member\AdvertsGalleryController@index'
        ]);
        Route::get('{listings_gal_pic}', [
            'as' => 'member.listings.gallery.fetch',
            'uses' => 'Member\AdvertsGalleryController@fetch'
        ]);
        Route::post('store', [
            'as' => 'member.listings.gallery.store',
            'uses' => 'Member\AdvertsGalleryController@store'
        ]);
        Route::post('save-sort', [
            'as' => 'member.listings.gallery.save_sort',
            'uses' => 'Member\AdvertsGalleryController@sort'
        ]);
        Route::match(['PUT', 'PATCH'], '{listings_gal_pic}', [
            'as' => 'member.listings.gallery.update',
            'uses' => 'Member\AdvertsGalleryController@update'
        ]);
        Route::delete('{listings_gal_pic}', [
            'as' => 'member.listings.gallery.destroy',
            'uses' => 'Member\AdvertsGalleryController@destroy'
        ]);
    });


    ////////////
    // Offers //
    ////////////

    
    Route::model('offer', 'Offer');

    // Offers converted to purchases
    Route::get('purchases', ['as' => 'member.offers.purchases', 'uses' => 'Member\OffersController@purchases']);
    Route::get('offers/payment-required', [
        'as' => 'member.offers.payment-required', 'uses' => 'Member\OffersController@paymentRequired'
    ]);
    Route::get('purchase-history', [
        'as' => 'member.offers.purchase-history', 'uses' => 'Member\OffersController@purchaseHistory'
    ]);

    // Raw offers, both actioned and unactioned
    Route::get('offers', ['as' => 'member.offers.index', 'uses' => 'Member\OffersController@index']);
    Route::get('offers/awaiting-response', [
        'as' => 'member.offers.awaiting_response', 'uses' => 'Member\OffersController@allAwaitingResponse'
    ]);
    Route::get('offers/actioned', ['as' => 'member.offers.actioned', 'uses' => 'Member\OffersController@allActioned']);

    // Action offers
    Route::get('offers/{offer}/accept', ['as' => 'member.offers.accept', 'uses' => 'Member\OffersController@accept']);
    Route::get('offers/{offer}/decline', [
        'as' => 'member.offers.decline', 'uses' => 'Member\OffersController@decline'
    ]);
    

    /////////////////////////
    // Activities ~ Alerts //
    /////////////////////////
    
    // The models and controllers are all still called activities, as they were
    // during dev. They were renamed to alerts near the end in URLs and labels only.
    
    Route::bind('alerts', function($value) {

        // Use ownedBySelf scope to prevent unauthorised editing
        return Activity::where('id', '=', $value)->ownedBySelf()->first();

    });
    Route::get('alerts/update-all', [
        'as' => 'member.alerts.update_all', 'uses' => 'Member\ActivitiesController@updateAll'
    ]);
    Route::get('alerts/delete-all', [
        'as' => 'member.alerts.delete_all', 'uses' => 'Member\ActivitiesController@destroyAll'
    ]);
    Route::get('alerts/{alerts}/mark-read', [
        'as' => 'member.alerts.mark_read', 'uses' => 'Member\ActivitiesController@markRead'
    ]);
    Route::resource('alerts', 'Member\ActivitiesController', ['only' => ['index', 'update', 'destroy']]);

});
