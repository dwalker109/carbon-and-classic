<?php

/**
 * Library class for Carbon & Classic's PayPal Adaptive Payments
 */

namespace CarbonAndClassic\AdaptivePayments;

// PayPal API
use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\FundingConstraint;
use PayPal\Types\AP\FundingTypeInfo;
use PayPal\Types\AP\FundingTypeList;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\PaymentDetailsRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
use PayPal\Types\AP\SenderIdentifier;
use PayPal\Types\Common\PhoneNumberType;
use PayPal\Types\Common\RequestEnvelope;
use PayPal\IPN\PPIPNMessage;

// Third party
use Confide;
use Uuid;

// Core Laravel
use Carbon\Carbon;
use Config;
use URL;
use Response;
use Input;
use Log;

// Domain
use Transaction;
use Advert;
use Offer;

// Core PHP
use DateInterval;

class AdaptivePayments
{
    // Rules for use in the pre-PayPal form (getter required for use via Facade)
    public static $rules = [
        'full_name' => ['required', 'max:255'],
        'address_line_1' => ['required', 'max:255'],
    ];


    // Class constants for dealing with PayPal messages
    const PP_ACTION_TYPE = 'PAY';
    const PP_CURRENCY_CODE = 'GBP';
    const PP_EXEC_STATUS_OK = 'CREATED';


    /**
     * Set up a purchase via PayPal and generate a URL to complete payment
     *
     * @param Advert $advert
     * @param Offer $offer
     * @param Collection $postage_option
     * @param string $memo
     * @return mixed
     */
    public function initiatePurchase($advert, $offer, $postage_option, $memo = null)
    {
        // Check validity of purchase
        if (self::validatePurchase($advert, $offer) !== true) {
            return false;
        }

        // Obtain the price to charge from the offer
        $purchase_price = $offer->offer_price + $postage_option->price;

        // Calculate C&C's 3% fee
        $site_fee = round($purchase_price * 0.03, 2);
        $site_fee = $site_fee < 3 ? 3.00 : $site_fee;
        $site_fee = $site_fee > 30 ? 30.00 : $site_fee;

        // Get some user details
        $user = Confide::user();

        // Setup Carbon & Classic as a receiver
        $site = new Receiver();
        $site->amount = $site_fee;
        $site->email = Config::get('paypal.email');

        // Setup seller as primary receiver: the total amount is paid to the
        // primary, and the fee is paid from them to the secondary
        $seller = new Receiver();
        $seller->amount = $purchase_price;
        $seller->email = $advert->owner->profile->paypal_id;
        $seller->primary = true;

        // Generate a tracking UUID, our handle on this payment request
        $paypal_tracking_id = Uuid::generate()->string;

        // Set a local expiry datetime to prevent concurrent purchase attempts,
        // which tallies with the validity of the PayPal pay request
        $paypal_request_expiry = Carbon::now()->add(
            new DateInterval(Config::get('paypal.pay_key_duration'))
        );

        // Form PayRequest
        $request = new PayRequest();
        $request->receiverList = new ReceiverList([$site, $seller]);
        $request->memo = $memo;
        $request->requestEnvelope = new RequestEnvelope('en_GB');
        $request->actionType = self::PP_ACTION_TYPE;
        $request->trackingId = $paypal_tracking_id;
        $request->cancelUrl = URL::route('checkout.cancel', [$paypal_tracking_id]);
        $request->returnUrl = URL::route('checkout.complete', [$paypal_tracking_id]);
        $request->ipnNotificationUrl = Config::get('paypal.ipn_url');
        $request->currencyCode = self::PP_CURRENCY_CODE;
        $request->reverseAllParallelPaymentsOnError = true;
        $request->payKeyDuration = Config::get('paypal.pay_key_duration');

        // Send the request
        $aps = new AdaptivePaymentsService(Config::get('paypal.sdk_config'));
        $response = $aps->Pay($request);

        // Payment was not created, early return
        if ($response->paymentExecStatus !== self::PP_EXEC_STATUS_OK) {
            Log::info('PayPal error', array('context' => $response));
            return false;
        }

        // Payment creation status is OK, save transaction locally
        $transaction = new Transaction([
            'expires_at' => $paypal_request_expiry,
            'purchase_price' => $purchase_price,
            'paypal_tracking_id' => $paypal_tracking_id,
            'paypal_status' => $response->paymentExecStatus,
        ]);

        // Associate the advert and purchaser
        $advert->transactions()->save($transaction);
        Confide::user()->transactions()->save($transaction);

        // Return the URL for redirecting to PayPal
        return Config::get('paypal.auth_url') . $response->payKey;
    }


    /**
     * Process a PayPal transaction on return (a completion or cancel)
     *
     * @param Transaction $transaction
     * @return void
     */
    public function processReturnFromPayPal($transaction)
    {
        // Obtain details of the PayPal transaction
        $request = new PaymentDetailsRequest(new RequestEnvelope('en_GB'));
        $request->trackingId = $transaction->paypal_tracking_id;
        $aps = new AdaptivePaymentsService(Config::get('paypal.sdk_config'));
        $payment = $aps->PaymentDetails($request);

        // Update the local transaction (advert purchase update will cascade from here)
        $transaction->fill([
            'paypal_status' => $payment->status,
            'paypal_sender_email' => $payment->senderEmail,
        ])->save();

        // Return a simplified success/fail bool for the view
        return $payment->status == Transaction::PP_TRANS_COMPLETE ? true : false;
    }


    /**
     * Handle IPN messages from PayPal
     *
     * @return Response
     */
    public function processIpn()
    {
        // Get the IPN data (comes straight from raw POST)
        $ipn_message = new PPIPNMessage(null, Config::get('paypal.sdk_config'));

        // Early exit with a HTTP 500 if IPN validation failed
        if (!$ipn_message->validate()) {
            return Response::make(null, 500);
        }

        // IPN validated OK with PayPal, update the transaction and send for processing
        $raw_data = (object) $ipn_message->getRawData();
        $transaction = Transaction::where(
            'paypal_tracking_id',
            '=',
            $raw_data->tracking_id
        )->firstOrFail();
        $this->processReturnFromPayPal($transaction);

        // Return a HTTP 200 to PayPal
        return Response::make(null, 200);
    }


    /**
     * Ensure that the advert is in a valid state to be purchased, and
     * that the offer being used applies to this advert. Will return an
     * error message if it fails, boolean true if it passes.
     *
     * @param  Advert $advert
     * @param  Offer $offer
     * @return boolean | string
     */
    public static function validatePurchase($advert, $offer)
    {
        // Reload models to ensure they are up to date
        $advert = Advert::find($advert->getKey());
        $offer = Offer::find($offer->getKey());

        // Ensure there is no ongoing or completed transaction
        if ($advert->purchase_in_progress) {
            return 'A checkout is already in progress';
        }
        if ($advert->winning_transaction) {
            return 'This item has already been paid for';
        }

        // Ensure that the offer is being used is for the current user
        // and the correct advert, and is still valid
        if ($offer->acceptance_status != Offer::OFFER_ACCEPTED
            || $offer->expires_at < Carbon::now()
            || $offer->initiator->id != Confide::user()->id
            || $offer->advert->id != $advert->id
        ) {
            return 'Your offer is invalid, or has expired';
        }

        // State is good
        return true;
    }


    /**
     * $rules getter for use via Facade
     *
     * @return array
     */
    public function getRules()
    {
        return self::$rules;
    }
}
