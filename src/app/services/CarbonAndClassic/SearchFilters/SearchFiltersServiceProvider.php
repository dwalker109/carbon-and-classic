<?php

namespace CarbonAndClassic\SearchFilters;

use Illuminate\Support\ServiceProvider;

class SearchFiltersServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'searchfilters' instance container to our SearchFilters object
        $this->app['searchfilters'] = $this->app->share(function($app) {
            return new SearchFilters;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias(
                'SearchFilters',
                'CarbonAndClassic\SearchFilters\Facades\SearchFilters'
            );
        });
    }
}
