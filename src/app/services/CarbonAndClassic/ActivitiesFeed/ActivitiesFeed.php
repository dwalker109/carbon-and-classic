<?php

namespace CarbonAndClassic\ActivitiesFeed;

// Laravel core
use Mail;
use Log;
use Config;

// Domain
use Activity;
use Role;

/**
 * Utility class for adding activity feeds for users and groups of
 * users (i.e. roles), also handling email sending as needed
 */

class ActivitiesFeed
{
    /**
     * Add an activity for a single user
     *
     * @param  User $user
     * @param  array $payload
     * @param  boolean|array $mail
     * @return boolean
     */
    public function add($user, $payload, $mail = false)
    {
        // Do not try to add to the feed if there is no user (i.e. non-logged in activity)
        if ($user) {
            try {
                $activity = Activity::create($payload);
                $user->activities()->save($activity);
            } catch (Exception $e) {
                Log::error('Could not add a notification', ['user' => $user, 'payload' => $payload]);
            }
        }

        // If mail var is boolean false - no emails at all, return early
        if ($mail === false) {
            return;
        }
        
        // Still here, send emails as well
        Mail::queue('emails.passthru', $payload, function($message) use ($mail, $user, $payload) {
            
            $message->subject($payload['title']);
            
            /* Recipients */

            if ($mail === true) {
                // $mail contains boolean true, use the user's email address alone
                $message->to($user->email);
            } else {
                // $mail contains an array, use its contents to call methods on the mailer
                foreach ($mail as $method => $value) {
                    $message->{$method}($value);
                }
            }
        });
    }


    /**
     * Add an activity for a role (normally admins)
     *
     * @param Role|integer $role
     * @param array $payload
     * @param  boolean|array $mail
     * @return boolean
     */
    public function addForRole($role, $payload, $mail = false)
    {
        // Expand a role id into a role object if needed
        if (!is_object($role)) {
            $role = Role::where('name', '=', $role)->firstOrFail();
        }

        try {
            $activity = Activity::create($payload);
            $role->activities()->save($activity);
        } catch (Exception $e) {
            Log::error('Could not add a notification', ['role' => $role, 'payload' => $payload]);
        }

        // If mail var is boolean false - no emails at all, return early
        if ($mail === false) {
            return;
        }
        
        // Still here, send emails as well
        Mail::queue('emails.passthru', $payload, function($message) use ($mail, $role, $payload) {
            
            $message->subject($payload['title']);

            /* Recipients */

            if ($mail === true) {
                // $mail contains boolean true, use the site admin email address alone
                $message->to(Config::get("domain.role_email.{$role->name}"));
            } else {
                // $mail contains an array, use its contents to call methods on the mailer
                foreach ($mail as $method => $value) {
                    $message->{$method}($value);
                }
            }
        });
    }
}
