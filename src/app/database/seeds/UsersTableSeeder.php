<?php

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        foreach (['admin', 'member'] as $usertype) {
            // Make user
            $user = new User;
            $user->username = "$usertype";
            $user->email = "$usertype@$usertype.com";
            $user->password = "$usertype";
            $user->password_confirmation = "$usertype";
            $user->confirmation_code = md5(uniqid(mt_rand(), true));
            $user->confirmed = true;
            $user->save();
            // Attach basic role for each user type
            $role = Role::where('Name', '=', ucfirst($usertype))->first();
            $user->attachRole($role);
        }
    }
}
