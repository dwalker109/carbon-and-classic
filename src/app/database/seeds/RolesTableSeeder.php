<?php

class RolesTableSeeder extends Seeder
{

    public function run()
    {
        foreach (Role::$base_roles as $base_role) {
            $role = new Role();
            $role->name = $base_role;
            $role->save();
        }
    }
}
