<?php

class CountriesTableSeeder extends Seeder
{
    public function run()
    {
        Country::create([
          'country_name' => 'Afghanistan',
          'alpha_2_code' => 'AF',
          'alpha_3_code' => 'AFG',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Åland Islands',
          'alpha_2_code' => 'AX',
          'alpha_3_code' => 'ALA',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Albania',
          'alpha_2_code' => 'AL',
          'alpha_3_code' => 'ALB',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Algeria',
          'alpha_2_code' => 'DZ',
          'alpha_3_code' => 'DZA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'American Samoa',
          'alpha_2_code' => 'AS',
          'alpha_3_code' => 'ASM',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Andorra',
          'alpha_2_code' => 'AD',
          'alpha_3_code' => 'AND',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Angola',
          'alpha_2_code' => 'AO',
          'alpha_3_code' => 'AGO',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Anguilla',
          'alpha_2_code' => 'AI',
          'alpha_3_code' => 'AIA',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Antarctica',
          'alpha_2_code' => 'AQ',
          'alpha_3_code' => 'ATA',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Antigua and Barbuda',
          'alpha_2_code' => 'AG',
          'alpha_3_code' => 'ATG',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Argentina',
          'alpha_2_code' => 'AR',
          'alpha_3_code' => 'ARG',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Armenia',
          'alpha_2_code' => 'AM',
          'alpha_3_code' => 'ARM',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Aruba',
          'alpha_2_code' => 'AW',
          'alpha_3_code' => 'ABW',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Australia',
          'alpha_2_code' => 'AU',
          'alpha_3_code' => 'AUS',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Australia and New Zealand',
        ]);

        Country::create([
          'country_name' => 'Austria',
          'alpha_2_code' => 'AT',
          'alpha_3_code' => 'AUT',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Azerbaijan',
          'alpha_2_code' => 'AZ',
          'alpha_3_code' => 'AZE',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Bahamas',
          'alpha_2_code' => 'BS',
          'alpha_3_code' => 'BHS',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Bahrain',
          'alpha_2_code' => 'BH',
          'alpha_3_code' => 'BHR',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Bangladesh',
          'alpha_2_code' => 'BD',
          'alpha_3_code' => 'BGD',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Barbados',
          'alpha_2_code' => 'BB',
          'alpha_3_code' => 'BRB',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Belarus',
          'alpha_2_code' => 'BY',
          'alpha_3_code' => 'BLR',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Belgium',
          'alpha_2_code' => 'BE',
          'alpha_3_code' => 'BEL',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Belize',
          'alpha_2_code' => 'BZ',
          'alpha_3_code' => 'BLZ',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Benin',
          'alpha_2_code' => 'BJ',
          'alpha_3_code' => 'BEN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Bermuda',
          'alpha_2_code' => 'BM',
          'alpha_3_code' => 'BMU',
          'region_name' => 'Americas',
          'sub_region_name' => 'Northern America',
        ]);

        Country::create([
          'country_name' => 'Bhutan',
          'alpha_2_code' => 'BT',
          'alpha_3_code' => 'BTN',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Bolivia, Plurinational State of',
          'alpha_2_code' => 'BO',
          'alpha_3_code' => 'BOL',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Bonaire, Sint Eustatius and Saba',
          'alpha_2_code' => 'BQ',
          'alpha_3_code' => 'BES',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Bosnia and Herzegovina',
          'alpha_2_code' => 'BA',
          'alpha_3_code' => 'BIH',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Botswana',
          'alpha_2_code' => 'BW',
          'alpha_3_code' => 'BWA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Southern Africa',
        ]);

        Country::create([
          'country_name' => 'Bouvet Island',
          'alpha_2_code' => 'BV',
          'alpha_3_code' => 'BVT',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Brazil',
          'alpha_2_code' => 'BR',
          'alpha_3_code' => 'BRA',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'British Indian Ocean Territory',
          'alpha_2_code' => 'IO',
          'alpha_3_code' => 'IOT',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Brunei Darussalam',
          'alpha_2_code' => 'BN',
          'alpha_3_code' => 'BRN',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Bulgaria',
          'alpha_2_code' => 'BG',
          'alpha_3_code' => 'BGR',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Burkina Faso',
          'alpha_2_code' => 'BF',
          'alpha_3_code' => 'BFA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Burundi',
          'alpha_2_code' => 'BI',
          'alpha_3_code' => 'BDI',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Cambodia',
          'alpha_2_code' => 'KH',
          'alpha_3_code' => 'KHM',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Cameroon',
          'alpha_2_code' => 'CM',
          'alpha_3_code' => 'CMR',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Canada',
          'alpha_2_code' => 'CA',
          'alpha_3_code' => 'CAN',
          'region_name' => 'Americas',
          'sub_region_name' => 'Northern America',
        ]);

        Country::create([
          'country_name' => 'Cape Verde',
          'alpha_2_code' => 'CV',
          'alpha_3_code' => 'CPV',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Cayman Islands',
          'alpha_2_code' => 'KY',
          'alpha_3_code' => 'CYM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Central African Republic',
          'alpha_2_code' => 'CF',
          'alpha_3_code' => 'CAF',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Chad',
          'alpha_2_code' => 'TD',
          'alpha_3_code' => 'TCD',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Chile',
          'alpha_2_code' => 'CL',
          'alpha_3_code' => 'CHL',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'China',
          'alpha_2_code' => 'CN',
          'alpha_3_code' => 'CHN',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Christmas Island',
          'alpha_2_code' => 'CX',
          'alpha_3_code' => 'CXR',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Cocos (Keeling) Islands',
          'alpha_2_code' => 'CC',
          'alpha_3_code' => 'CCK',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Colombia',
          'alpha_2_code' => 'CO',
          'alpha_3_code' => 'COL',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Comoros',
          'alpha_2_code' => 'KM',
          'alpha_3_code' => 'COM',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Congo',
          'alpha_2_code' => 'CG',
          'alpha_3_code' => 'COG',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Congo, the Democratic Republic of the',
          'alpha_2_code' => 'CD',
          'alpha_3_code' => 'COD',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Cook Islands',
          'alpha_2_code' => 'CK',
          'alpha_3_code' => 'COK',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Costa Rica',
          'alpha_2_code' => 'CR',
          'alpha_3_code' => 'CRI',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Côte d`Ivoire',
          'alpha_2_code' => 'CI',
          'alpha_3_code' => 'CIV',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Croatia',
          'alpha_2_code' => 'HR',
          'alpha_3_code' => 'HRV',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Cuba',
          'alpha_2_code' => 'CU',
          'alpha_3_code' => 'CUB',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Curaçao',
          'alpha_2_code' => 'CW',
          'alpha_3_code' => 'CUW',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Cyprus',
          'alpha_2_code' => 'CY',
          'alpha_3_code' => 'CYP',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Czech Republic',
          'alpha_2_code' => 'CZ',
          'alpha_3_code' => 'CZE',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Denmark',
          'alpha_2_code' => 'DK',
          'alpha_3_code' => 'DNK',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Djibouti',
          'alpha_2_code' => 'DJ',
          'alpha_3_code' => 'DJI',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Dominica',
          'alpha_2_code' => 'DM',
          'alpha_3_code' => 'DMA',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Dominican Republic',
          'alpha_2_code' => 'DO',
          'alpha_3_code' => 'DOM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Ecuador',
          'alpha_2_code' => 'EC',
          'alpha_3_code' => 'ECU',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Egypt',
          'alpha_2_code' => 'EG',
          'alpha_3_code' => 'EGY',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'El Salvador',
          'alpha_2_code' => 'SV',
          'alpha_3_code' => 'SLV',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Equatorial Guinea',
          'alpha_2_code' => 'GQ',
          'alpha_3_code' => 'GNQ',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Eritrea',
          'alpha_2_code' => 'ER',
          'alpha_3_code' => 'ERI',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Estonia',
          'alpha_2_code' => 'EE',
          'alpha_3_code' => 'EST',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Ethiopia',
          'alpha_2_code' => 'ET',
          'alpha_3_code' => 'ETH',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Falkland Islands (Malvinas)',
          'alpha_2_code' => 'FK',
          'alpha_3_code' => 'FLK',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Faroe Islands',
          'alpha_2_code' => 'FO',
          'alpha_3_code' => 'FRO',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Fiji',
          'alpha_2_code' => 'FJ',
          'alpha_3_code' => 'FJI',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Melanesia',
        ]);

        Country::create([
          'country_name' => 'Finland',
          'alpha_2_code' => 'FI',
          'alpha_3_code' => 'FIN',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'France',
          'alpha_2_code' => 'FR',
          'alpha_3_code' => 'FRA',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'French Guiana',
          'alpha_2_code' => 'GF',
          'alpha_3_code' => 'GUF',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'French Polynesia',
          'alpha_2_code' => 'PF',
          'alpha_3_code' => 'PYF',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'French Southern Territories',
          'alpha_2_code' => 'TF',
          'alpha_3_code' => 'ATF',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Gabon',
          'alpha_2_code' => 'GA',
          'alpha_3_code' => 'GAB',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Gambia',
          'alpha_2_code' => 'GM',
          'alpha_3_code' => 'GMB',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Georgia',
          'alpha_2_code' => 'GE',
          'alpha_3_code' => 'GEO',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Germany',
          'alpha_2_code' => 'DE',
          'alpha_3_code' => 'DEU',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Ghana',
          'alpha_2_code' => 'GH',
          'alpha_3_code' => 'GHA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Gibraltar',
          'alpha_2_code' => 'GI',
          'alpha_3_code' => 'GIB',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Greece',
          'alpha_2_code' => 'GR',
          'alpha_3_code' => 'GRC',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Greenland',
          'alpha_2_code' => 'GL',
          'alpha_3_code' => 'GRL',
          'region_name' => 'Americas',
          'sub_region_name' => 'Northern America',
        ]);

        Country::create([
          'country_name' => 'Grenada',
          'alpha_2_code' => 'GD',
          'alpha_3_code' => 'GRD',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Guadeloupe',
          'alpha_2_code' => 'GP',
          'alpha_3_code' => 'GLP',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Guam',
          'alpha_2_code' => 'GU',
          'alpha_3_code' => 'GUM',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Guatemala',
          'alpha_2_code' => 'GT',
          'alpha_3_code' => 'GTM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Guernsey',
          'alpha_2_code' => 'GG',
          'alpha_3_code' => 'GGY',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Guinea',
          'alpha_2_code' => 'GN',
          'alpha_3_code' => 'GIN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Guinea-Bissau',
          'alpha_2_code' => 'GW',
          'alpha_3_code' => 'GNB',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Guyana',
          'alpha_2_code' => 'GY',
          'alpha_3_code' => 'GUY',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Haiti',
          'alpha_2_code' => 'HT',
          'alpha_3_code' => 'HTI',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Heard Island and McDonald Islands',
          'alpha_2_code' => 'HM',
          'alpha_3_code' => 'HMD',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Holy See (Vatican City State)',
          'alpha_2_code' => 'VA',
          'alpha_3_code' => 'VAT',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Honduras',
          'alpha_2_code' => 'HN',
          'alpha_3_code' => 'HND',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Hong Kong',
          'alpha_2_code' => 'HK',
          'alpha_3_code' => 'HKG',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Hungary',
          'alpha_2_code' => 'HU',
          'alpha_3_code' => 'HUN',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Iceland',
          'alpha_2_code' => 'IS',
          'alpha_3_code' => 'ISL',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'India',
          'alpha_2_code' => 'IN',
          'alpha_3_code' => 'IND',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Indonesia',
          'alpha_2_code' => 'ID',
          'alpha_3_code' => 'IDN',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Iran, Islamic Republic of',
          'alpha_2_code' => 'IR',
          'alpha_3_code' => 'IRN',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Iraq',
          'alpha_2_code' => 'IQ',
          'alpha_3_code' => 'IRQ',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Ireland',
          'alpha_2_code' => 'IE',
          'alpha_3_code' => 'IRL',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Isle of Man',
          'alpha_2_code' => 'IM',
          'alpha_3_code' => 'IMN',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Israel',
          'alpha_2_code' => 'IL',
          'alpha_3_code' => 'ISR',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Italy',
          'alpha_2_code' => 'IT',
          'alpha_3_code' => 'ITA',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Jamaica',
          'alpha_2_code' => 'JM',
          'alpha_3_code' => 'JAM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Japan',
          'alpha_2_code' => 'JP',
          'alpha_3_code' => 'JPN',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Jersey',
          'alpha_2_code' => 'JE',
          'alpha_3_code' => 'JEY',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Jordan',
          'alpha_2_code' => 'JO',
          'alpha_3_code' => 'JOR',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Kazakhstan',
          'alpha_2_code' => 'KZ',
          'alpha_3_code' => 'KAZ',
          'region_name' => 'Asia',
          'sub_region_name' => 'Central Asia',
        ]);

        Country::create([
          'country_name' => 'Kenya',
          'alpha_2_code' => 'KE',
          'alpha_3_code' => 'KEN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Kiribati',
          'alpha_2_code' => 'KI',
          'alpha_3_code' => 'KIR',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Korea, Democratic People`s Republic of',
          'alpha_2_code' => 'KP',
          'alpha_3_code' => 'PRK',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Korea, Republic of',
          'alpha_2_code' => 'KR',
          'alpha_3_code' => 'KOR',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Kuwait',
          'alpha_2_code' => 'KW',
          'alpha_3_code' => 'KWT',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Kyrgyzstan',
          'alpha_2_code' => 'KG',
          'alpha_3_code' => 'KGZ',
          'region_name' => 'Asia',
          'sub_region_name' => 'Central Asia',
        ]);

        Country::create([
          'country_name' => 'Lao People`s Democratic Republic',
          'alpha_2_code' => 'LA',
          'alpha_3_code' => 'LAO',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Latvia',
          'alpha_2_code' => 'LV',
          'alpha_3_code' => 'LVA',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Lebanon',
          'alpha_2_code' => 'LB',
          'alpha_3_code' => 'LBN',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Lesotho',
          'alpha_2_code' => 'LS',
          'alpha_3_code' => 'LSO',
          'region_name' => 'Africa',
          'sub_region_name' => 'Southern Africa',
        ]);

        Country::create([
          'country_name' => 'Liberia',
          'alpha_2_code' => 'LR',
          'alpha_3_code' => 'LBR',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Libyan Arab Jamahiriya',
          'alpha_2_code' => 'LY',
          'alpha_3_code' => 'LBY',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'Liechtenstein',
          'alpha_2_code' => 'LI',
          'alpha_3_code' => 'LIE',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Lithuania',
          'alpha_2_code' => 'LT',
          'alpha_3_code' => 'LTU',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Luxembourg',
          'alpha_2_code' => 'LU',
          'alpha_3_code' => 'LUX',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Macao',
          'alpha_2_code' => 'MO',
          'alpha_3_code' => 'MAC',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Macedonia, the former Yugoslav Republic of',
          'alpha_2_code' => 'MK',
          'alpha_3_code' => 'MKD',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Madagascar',
          'alpha_2_code' => 'MG',
          'alpha_3_code' => 'MDG',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Malawi',
          'alpha_2_code' => 'MW',
          'alpha_3_code' => 'MWI',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Malaysia',
          'alpha_2_code' => 'MY',
          'alpha_3_code' => 'MYS',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Maldives',
          'alpha_2_code' => 'MV',
          'alpha_3_code' => 'MDV',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Mali',
          'alpha_2_code' => 'ML',
          'alpha_3_code' => 'MLI',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Malta',
          'alpha_2_code' => 'MT',
          'alpha_3_code' => 'MLT',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Marshall Islands',
          'alpha_2_code' => 'MH',
          'alpha_3_code' => 'MHL',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Martinique',
          'alpha_2_code' => 'MQ',
          'alpha_3_code' => 'MTQ',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Mauritania',
          'alpha_2_code' => 'MR',
          'alpha_3_code' => 'MRT',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Mauritius',
          'alpha_2_code' => 'MU',
          'alpha_3_code' => 'MUS',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Mayotte',
          'alpha_2_code' => 'YT',
          'alpha_3_code' => 'MYT',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Mexico',
          'alpha_2_code' => 'MX',
          'alpha_3_code' => 'MEX',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Micronesia, Federated States of',
          'alpha_2_code' => 'FM',
          'alpha_3_code' => 'FSM',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Moldova, Republic of',
          'alpha_2_code' => 'MD',
          'alpha_3_code' => 'MDA',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Monaco',
          'alpha_2_code' => 'MC',
          'alpha_3_code' => 'MCO',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Mongolia',
          'alpha_2_code' => 'MN',
          'alpha_3_code' => 'MNG',
          'region_name' => 'Asia',
          'sub_region_name' => 'Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Montenegro',
          'alpha_2_code' => 'ME',
          'alpha_3_code' => 'MNE',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Montserrat',
          'alpha_2_code' => 'MS',
          'alpha_3_code' => 'MSR',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Morocco',
          'alpha_2_code' => 'MA',
          'alpha_3_code' => 'MAR',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'Mozambique',
          'alpha_2_code' => 'MZ',
          'alpha_3_code' => 'MOZ',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Myanmar',
          'alpha_2_code' => 'MM',
          'alpha_3_code' => 'MMR',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Namibia',
          'alpha_2_code' => 'NA',
          'alpha_3_code' => 'NAM',
          'region_name' => 'Africa',
          'sub_region_name' => 'Southern Africa',
        ]);

        Country::create([
          'country_name' => 'Nauru',
          'alpha_2_code' => 'NR',
          'alpha_3_code' => 'NRU',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Nepal',
          'alpha_2_code' => 'NP',
          'alpha_3_code' => 'NPL',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Netherlands',
          'alpha_2_code' => 'NL',
          'alpha_3_code' => 'NLD',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'New Caledonia',
          'alpha_2_code' => 'NC',
          'alpha_3_code' => 'NCL',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Melanesia',
        ]);

        Country::create([
          'country_name' => 'New Zealand',
          'alpha_2_code' => 'NZ',
          'alpha_3_code' => 'NZL',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Australia and New Zealand',
        ]);

        Country::create([
          'country_name' => 'Nicaragua',
          'alpha_2_code' => 'NI',
          'alpha_3_code' => 'NIC',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Niger',
          'alpha_2_code' => 'NE',
          'alpha_3_code' => 'NER',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Nigeria',
          'alpha_2_code' => 'NG',
          'alpha_3_code' => 'NGA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Niue',
          'alpha_2_code' => 'NU',
          'alpha_3_code' => 'NIU',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Norfolk Island',
          'alpha_2_code' => 'NF',
          'alpha_3_code' => 'NFK',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Australia and New Zealand',
        ]);

        Country::create([
          'country_name' => 'Northern Mariana Islands',
          'alpha_2_code' => 'MP',
          'alpha_3_code' => 'MNP',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Norway',
          'alpha_2_code' => 'NO',
          'alpha_3_code' => 'NOR',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Oman',
          'alpha_2_code' => 'OM',
          'alpha_3_code' => 'OMN',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Pakistan',
          'alpha_2_code' => 'PK',
          'alpha_3_code' => 'PAK',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Palau',
          'alpha_2_code' => 'PW',
          'alpha_3_code' => 'PLW',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Micronesia',
        ]);

        Country::create([
          'country_name' => 'Palestinian Territory, Occupied',
          'alpha_2_code' => 'PS',
          'alpha_3_code' => 'PSE',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Panama',
          'alpha_2_code' => 'PA',
          'alpha_3_code' => 'PAN',
          'region_name' => 'Americas',
          'sub_region_name' => 'Central America',
        ]);

        Country::create([
          'country_name' => 'Papua New Guinea',
          'alpha_2_code' => 'PG',
          'alpha_3_code' => 'PNG',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Melanesia',
        ]);

        Country::create([
          'country_name' => 'Paraguay',
          'alpha_2_code' => 'PY',
          'alpha_3_code' => 'PRY',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Peru',
          'alpha_2_code' => 'PE',
          'alpha_3_code' => 'PER',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Philippines',
          'alpha_2_code' => 'PH',
          'alpha_3_code' => 'PHL',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Pitcairn',
          'alpha_2_code' => 'PN',
          'alpha_3_code' => 'PCN',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Poland',
          'alpha_2_code' => 'PL',
          'alpha_3_code' => 'POL',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Portugal',
          'alpha_2_code' => 'PT',
          'alpha_3_code' => 'PRT',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Puerto Rico',
          'alpha_2_code' => 'PR',
          'alpha_3_code' => 'PRI',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Qatar',
          'alpha_2_code' => 'QA',
          'alpha_3_code' => 'QAT',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Réunion',
          'alpha_2_code' => 'RE',
          'alpha_3_code' => 'REU',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Romania',
          'alpha_2_code' => 'RO',
          'alpha_3_code' => 'ROU',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Russian Federation',
          'alpha_2_code' => 'RU',
          'alpha_3_code' => 'RUS',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Rwanda',
          'alpha_2_code' => 'RW',
          'alpha_3_code' => 'RWA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Saint Barthélemy',
          'alpha_2_code' => 'BL',
          'alpha_3_code' => 'BLM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Saint Helena, Ascension and Tristan da Cunha',
          'alpha_2_code' => 'SH',
          'alpha_3_code' => 'SHN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Saint Kitts and Nevis',
          'alpha_2_code' => 'KN',
          'alpha_3_code' => 'KNA',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Saint Lucia',
          'alpha_2_code' => 'LC',
          'alpha_3_code' => 'LCA',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Saint Martin (French part)',
          'alpha_2_code' => 'MF',
          'alpha_3_code' => 'MAF',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Saint Pierre and Miquelon',
          'alpha_2_code' => 'PM',
          'alpha_3_code' => 'SPM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Northern America',
        ]);

        Country::create([
          'country_name' => 'Saint Vincent and the Grenadines',
          'alpha_2_code' => 'VC',
          'alpha_3_code' => 'VCT',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Samoa',
          'alpha_2_code' => 'WS',
          'alpha_3_code' => 'WSM',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'San Marino',
          'alpha_2_code' => 'SM',
          'alpha_3_code' => 'SMR',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Sao Tome and Principe',
          'alpha_2_code' => 'ST',
          'alpha_3_code' => 'STP',
          'region_name' => 'Africa',
          'sub_region_name' => 'Middle Africa',
        ]);

        Country::create([
          'country_name' => 'Saudi Arabia',
          'alpha_2_code' => 'SA',
          'alpha_3_code' => 'SAU',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Senegal',
          'alpha_2_code' => 'SN',
          'alpha_3_code' => 'SEN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Serbia',
          'alpha_2_code' => 'RS',
          'alpha_3_code' => 'SRB',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Seychelles',
          'alpha_2_code' => 'SC',
          'alpha_3_code' => 'SYC',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Sierra Leone',
          'alpha_2_code' => 'SL',
          'alpha_3_code' => 'SLE',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Singapore',
          'alpha_2_code' => 'SG',
          'alpha_3_code' => 'SGP',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Sint Maarten (Dutch part)',
          'alpha_2_code' => 'SX',
          'alpha_3_code' => 'SXM',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Slovakia',
          'alpha_2_code' => 'SK',
          'alpha_3_code' => 'SVK',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'Slovenia',
          'alpha_2_code' => 'SI',
          'alpha_3_code' => 'SVN',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Solomon Islands',
          'alpha_2_code' => 'SB',
          'alpha_3_code' => 'SLB',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Melanesia',
        ]);

        Country::create([
          'country_name' => 'Somalia',
          'alpha_2_code' => 'SO',
          'alpha_3_code' => 'SOM',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'South Africa',
          'alpha_2_code' => 'ZA',
          'alpha_3_code' => 'ZAF',
          'region_name' => 'Africa',
          'sub_region_name' => 'Southern Africa',
        ]);

        Country::create([
          'country_name' => 'South Georgia and the South Sandwich Islands',
          'alpha_2_code' => 'GS',
          'alpha_3_code' => 'SGS',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'South Sudan',
          'alpha_2_code' => 'SS',
          'alpha_3_code' => 'SSD',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'Spain',
          'alpha_2_code' => 'ES',
          'alpha_3_code' => 'ESP',
          'region_name' => 'Europe',
          'sub_region_name' => 'Southern Europe',
        ]);

        Country::create([
          'country_name' => 'Sri Lanka',
          'alpha_2_code' => 'LK',
          'alpha_3_code' => 'LKA',
          'region_name' => 'Asia',
          'sub_region_name' => 'Southern Asia',
        ]);

        Country::create([
          'country_name' => 'Sudan',
          'alpha_2_code' => 'SD',
          'alpha_3_code' => 'SDN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'Suriname',
          'alpha_2_code' => 'SR',
          'alpha_3_code' => 'SUR',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Svalbard and Jan Mayen',
          'alpha_2_code' => 'SJ',
          'alpha_3_code' => 'SJM',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Swaziland',
          'alpha_2_code' => 'SZ',
          'alpha_3_code' => 'SWZ',
          'region_name' => 'Africa',
          'sub_region_name' => 'Southern Africa',
        ]);

        Country::create([
          'country_name' => 'Sweden',
          'alpha_2_code' => 'SE',
          'alpha_3_code' => 'SWE',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'Switzerland',
          'alpha_2_code' => 'CH',
          'alpha_3_code' => 'CHE',
          'region_name' => 'Europe',
          'sub_region_name' => 'Western Europe',
        ]);

        Country::create([
          'country_name' => 'Syrian Arab Republic',
          'alpha_2_code' => 'SY',
          'alpha_3_code' => 'SYR',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Taiwan, Province of China',
          'alpha_2_code' => 'TW',
          'alpha_3_code' => 'TWN',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Tajikistan',
          'alpha_2_code' => 'TJ',
          'alpha_3_code' => 'TJK',
          'region_name' => 'Asia',
          'sub_region_name' => 'Central Asia',
        ]);

        Country::create([
          'country_name' => 'Tanzania, United Republic of',
          'alpha_2_code' => 'TZ',
          'alpha_3_code' => 'TZA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Thailand',
          'alpha_2_code' => 'TH',
          'alpha_3_code' => 'THA',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Timor-Leste',
          'alpha_2_code' => 'TL',
          'alpha_3_code' => 'TLS',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Togo',
          'alpha_2_code' => 'TG',
          'alpha_3_code' => 'TGO',
          'region_name' => 'Africa',
          'sub_region_name' => 'Western Africa',
        ]);

        Country::create([
          'country_name' => 'Tokelau',
          'alpha_2_code' => 'TK',
          'alpha_3_code' => 'TKL',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Tonga',
          'alpha_2_code' => 'TO',
          'alpha_3_code' => 'TON',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Trinidad and Tobago',
          'alpha_2_code' => 'TT',
          'alpha_3_code' => 'TTO',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Tunisia',
          'alpha_2_code' => 'TN',
          'alpha_3_code' => 'TUN',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'Turkey',
          'alpha_2_code' => 'TR',
          'alpha_3_code' => 'TUR',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Turkmenistan',
          'alpha_2_code' => 'TM',
          'alpha_3_code' => 'TKM',
          'region_name' => 'Asia',
          'sub_region_name' => 'Central Asia',
        ]);

        Country::create([
          'country_name' => 'Turks and Caicos Islands',
          'alpha_2_code' => 'TC',
          'alpha_3_code' => 'TCA',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Tuvalu',
          'alpha_2_code' => 'TV',
          'alpha_3_code' => 'TUV',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Uganda',
          'alpha_2_code' => 'UG',
          'alpha_3_code' => 'UGA',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Ukraine',
          'alpha_2_code' => 'UA',
          'alpha_3_code' => 'UKR',
          'region_name' => 'Europe',
          'sub_region_name' => 'Eastern Europe',
        ]);

        Country::create([
          'country_name' => 'United Arab Emirates',
          'alpha_2_code' => 'AE',
          'alpha_3_code' => 'ARE',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'United Kingdom',
          'alpha_2_code' => 'GB',
          'alpha_3_code' => 'GBR',
          'region_name' => 'Europe',
          'sub_region_name' => 'Northern Europe',
        ]);

        Country::create([
          'country_name' => 'United States',
          'alpha_2_code' => 'US',
          'alpha_3_code' => 'USA',
          'region_name' => 'Americas',
          'sub_region_name' => 'Northern America',
        ]);

        Country::create([
          'country_name' => 'United States Minor Outlying Islands',
          'alpha_2_code' => 'UM',
          'alpha_3_code' => 'UMI',
          'region_name' => '',
          'sub_region_name' => '',
        ]);

        Country::create([
          'country_name' => 'Uruguay',
          'alpha_2_code' => 'UY',
          'alpha_3_code' => 'URY',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Uzbekistan',
          'alpha_2_code' => 'UZ',
          'alpha_3_code' => 'UZB',
          'region_name' => 'Asia',
          'sub_region_name' => 'Central Asia',
        ]);

        Country::create([
          'country_name' => 'Vanuatu',
          'alpha_2_code' => 'VU',
          'alpha_3_code' => 'VUT',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Melanesia',
        ]);

        Country::create([
          'country_name' => 'Venezuela, Bolivarian Republic of',
          'alpha_2_code' => 'VE',
          'alpha_3_code' => 'VEN',
          'region_name' => 'Americas',
          'sub_region_name' => 'South America',
        ]);

        Country::create([
          'country_name' => 'Viet Nam',
          'alpha_2_code' => 'VN',
          'alpha_3_code' => 'VNM',
          'region_name' => 'Asia',
          'sub_region_name' => 'South-Eastern Asia',
        ]);

        Country::create([
          'country_name' => 'Virgin Islands, British',
          'alpha_2_code' => 'VG',
          'alpha_3_code' => 'VGB',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Virgin Islands, U.S.',
          'alpha_2_code' => 'VI',
          'alpha_3_code' => 'VIR',
          'region_name' => 'Americas',
          'sub_region_name' => 'Latin America and the Caribbean',
        ]);

        Country::create([
          'country_name' => 'Wallis and Futuna',
          'alpha_2_code' => 'WF',
          'alpha_3_code' => 'WLF',
          'region_name' => 'Oceania',
          'sub_region_name' => 'Polynesia',
        ]);

        Country::create([
          'country_name' => 'Western Sahara',
          'alpha_2_code' => 'EH',
          'alpha_3_code' => 'ESH',
          'region_name' => 'Africa',
          'sub_region_name' => 'Northern Africa',
        ]);

        Country::create([
          'country_name' => 'Yemen',
          'alpha_2_code' => 'YE',
          'alpha_3_code' => 'YEM',
          'region_name' => 'Asia',
          'sub_region_name' => 'Western Asia',
        ]);

        Country::create([
          'country_name' => 'Zambia',
          'alpha_2_code' => 'ZM',
          'alpha_3_code' => 'ZMB',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);

        Country::create([
          'country_name' => 'Zimbabwe',
          'alpha_2_code' => 'ZW',
          'alpha_3_code' => 'ZWE',
          'region_name' => 'Africa',
          'sub_region_name' => 'Eastern Africa',
        ]);
    }
}
