<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // Seeds required for the app to function
        $seeds = [
            'RolesTableSeeder',
            'UsersTableSeeder',
            'PermissionsTableSeeder',
            'CountriesTableSeeder',
            'CategoriesTableSeeder',
            'ManufacturersTableSeeder',
        ];

        // Dummy data for local dev
        $dummy_seeds = [];

        if (App::environment() === 'local') {
            $seeds = array_merge($seeds, $dummy_seeds);
        }

        foreach ($seeds as $seed) {
            $this->call($seed);
        }
    }
}
