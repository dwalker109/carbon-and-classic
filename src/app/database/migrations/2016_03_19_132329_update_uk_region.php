<?php

use Illuminate\Database\Migrations\Migration;

class UpdateUkRegion extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        DB::table('countries')
            ->where('country_name', 'United Kingdom')
            ->update(array('region_name' => 'United Kingdom'));
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        DB::table('countries')
            ->where('country_name', 'United Kingdom')
            ->update(array('region_name' => 'Europe'));
    }
}
