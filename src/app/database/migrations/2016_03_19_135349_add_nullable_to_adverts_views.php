<?php

use Illuminate\Database\Migrations\Migration;

class AddNullableToAdvertsViews extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        DB::statement("ALTER TABLE `adverts` CHANGE COLUMN `views` `views` DECIMAL(8,2) UNSIGNED NOT NULL DEFAULT 0 COMMENT ''");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        // Nah
    }
}
