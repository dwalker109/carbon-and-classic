<?php

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class MessagesController extends BaseController
{

    /**
     * Show all of the message threads to the user
     *
     * @return mixed
     */
    public function index()
    {
        $threads = Thread::forUser(Confide::user()->id)->orderBy('updated_at', 'DESC')->get();
        return View::make('messenger.index', compact('threads'));
    }

    /**
     * Shows a message thread
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Notification::error('The conversation was not found');
            return Redirect::route('messages');
        }

        $users = User::whereIn('id', $thread->participantsUserIds())->get()->keyBy('id');

        if (! $users->has(Confide::user()->id)) {
            Notification::error('You do not have permission to view that conversation.');
            return Redirect::to('messages');
        }

        $thread->markAsRead(Confide::user()->id);

        return View::make('messenger.show', compact('thread', 'users'));
    }

    /**
     * Creates a new message thread
     *
     * @param $username
     * @return mixed
     */
    public function create($username)
    {
        $recipient = User::where('username', '=', $username)->firstOrFail();

        return View::make('messenger.create', compact('recipient'));
    }

    /**
     * Stores a new message thread
     *
     * @return mixed
     */
    public function store()
    {
        // Thread
        $thread = Thread::create([
            'subject' => Input::get('subject'),
        ]);

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id'   => Confide::user()->id,
            'body'      => Input::get('message'),
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id'   => Confide::user()->id,
            'last_read' => new Carbon,
        ]);

        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipants(Input::get('recipients'));
        }

        return Redirect::route('messages');
    }

    /**
     * Adds a new message to a current thread
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Notification::error('The conversation was not found.');
            return Redirect::to('messages');
        }

        $thread->activateAllParticipants();

        // Throw an error if replier is not a participant
        try {
            $participant = Participant::whereUserId(Confide::user()->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            Notification::error('You do not have permission to reply to that conversation.');
            return Redirect::to('messages');
        }

        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id'   => Auth::id(),
            'body'      => Input::get('message'),
        ]);

        // Update last read
        $participant->last_read = new Carbon;
        $participant->save();

        return Redirect::route('messages.show', ['id' => $id]);
    }
}
