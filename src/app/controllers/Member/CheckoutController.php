<?php

namespace Member;

// Core
use Confide;
use View;
use Response;
use Redirect;
use Input;
// Third Party
use Notification;
// Domain
use AdaptivePayments;
use Offer;
use ActivitiesFeed;

class CheckoutController extends \BaseController
{
    /**
     * Start the checkout process - display the confirmation page.
     *
     * @param Offer $offer
     *
     * @return Response
     */
    public function start($offer)
    {
        // Retrieve advert from the offer
        $advert = $offer->advert;

        // Ensure some delivery options are available before proceeding
        if (!$advert->is_deliverable_to_user) {
            Notification::error('There are no applicable postage options for this item. '
                .link_to_route('messages.create', 'Contact the seller', $advert->owner->username));

            return Redirect::back();
        }

        $profile = Confide::user()->profile;

        return View::make('member.checkout.start', compact('advert', 'offer', 'profile'));
    }

    /**
     * User has confirmed the transaction so carry out the payment.
     *
     * @return Response
     */
    public function process($offer)
    {
        // Retrieve advert from the offer
        $advert = $offer->advert;

        // Ensure a valid postage option has been selected
        $postage_option = $advert->user_applicable_postage_options->get(
            Input::get('postage_option')
        );
        if (!$postage_option) {
            Notification::error('Please select a delivery option');

            return Redirect::back();
        }

        // Ensure the advert and offer are valid, early return if not
        $purchaseValid = AdaptivePayments::validatePurchase($advert, $offer);
        if ($purchaseValid !== true) {
            Notification::errorInstant($purchaseValid);

            return View::make('member.checkout.error');
        }

        // Craft a 'memo' containing entered address data and chosen shipping options
        $memo = View::make('member.checkout.memo', compact('postage_option'))->render();

        // Try to initiate the PayPal payment and get a URL to complete
        $paypal_purchase_url = AdaptivePayments::initiatePurchase($advert, $offer, $postage_option, $memo);

        if ($paypal_purchase_url) {
            return Redirect::secure($paypal_purchase_url);
        } else {
            Notification::errorInstant('Error authorising with PayPal. Please contact us for assistance.');

            return View::make('member.checkout.error');
        }
    }

    /**
     * Complete the checkout on return from PayPal.
     *
     * @param Transaction $transaction
     *
     * @return Response
     */
    public function complete($transaction)
    {
        $success = AdaptivePayments::processReturnFromPayPal($transaction);

        if ($success) {
            Notification::success(
                'Your purchase is complete - details of your completed purchases are below'
            );

            return Redirect::route('member.offers.purchase-history');
        } else {
            Notification::error(
                'An unknown error occurred during checkout - please contact us for assistance'
            );

            return View::make('member.checkout.error');
        }
    }

    /**
     * Cancel the checkout on return from PayPal.
     *
     * @param Transaction $transaction
     *
     * @return Response
     */
    public function cancel($transaction)
    {
        AdaptivePayments::processReturnFromPayPal($transaction);

        return Redirect::Route('member.offers.payment-required');
    }

    /**
     * Process IPN responses from PayPal.
     */
    public function ipn()
    {
        AdaptivePayments::processIpn();
    }

    ////////////
    // Offers //
    ////////////

    /**
     * Process an offer on an advert.
     *
     * @param Advert $advert
     *
     * @return Response
     */
    public function makeOffer($advert)
    {
        // Do not allow if delivery is not available to this user
        if (!$advert->is_deliverable_to_user) {
            Notification::error(
                'Sorry, delivery is not available to your region so you cannot make an offer'
            );

            return Redirect::back();
        }

        // Do not allow if advert is owned by current user
        if ($advert->owner->id === Confide::user()->id) {
            Notification::error(
                'Sorry, you cannot make an offer on your own listing'
            );

            return Redirect::back();
        }

        // Do not allow if offer is less than 1 pence
        if ((float) Input::get('offer_price') < 0.01) {
            Notification::error(
                'Sorry, you cannott make an offer of less than £0.01'
            );

            return Redirect::back();
        }

        $offer = new Offer();

        if ($offer->fill(['offer_price' => Input::get('offer_price')])->save()) {

            // Saved offer, associate with advert and user, add alert
            $advert->offers()->save($offer);
            Confide::user()->offersMade()->save($offer);

            $payload = [
                'title' => 'An offer has been made on one of your adverts',
                'body' => View::make(
                    'emails.adverts.offer_made',
                    compact('advert', 'offer')
                )->render(),
            ];
            ActivitiesFeed::add($advert->owner, $payload, $send_mail = true);

            Notification::info('You offer has been submitted, sit tight!');

            return Redirect::route('public.advert', ['advert' => $advert->slug]);
        } else {

            // Save failed, redirect with errors
            return Redirect::route('public.advert', ['advert' => $advert->slug])
                ->withErrors($offer->getErrors())->withInput();
        }
    }

    /**
     * Process an offer instantly marked as accepted (an intent to purchase at
     * the full requested price).
     *
     * @param Advert $advert
     *
     * @return Response
     */
    public function makeAutoSuccessOffer($advert)
    {

        // Do not allow if delivery is not available to this user
        if (!$advert->is_deliverable_to_user) {
            Notification::error(
                'Sorry, delivery is not available to your region so you cannot purchase this item'
            );

            return Redirect::back();
        }

        // Do not allow if advert is owned by current user
        if ($advert->owner->id === Confide::user()->id) {
            Notification::error(
                'Sorry, you cannot make an offer on your own listing'
            );

            return Redirect::back();
        }

        $offer = new Offer();

        if ($offer->fill(['offer_price' => $advert->price])->save()) {

            // Saved offer, associate with advert and user
            $advert->offers()->save($offer);
            Confide::user()->offersMade()->save($offer);

            // Set the status to accepted
            $offer->fill(['acceptance_status' => Offer::OFFER_ACCEPTED])->save();

            Notification::info('Your offer to purchase has been accepted');

            return Redirect::route('member.offers.payment-required');
        } else {

            // Save failed, redirect with errors
            return Redirect::route('public.advert', ['advert' => $advert->slug])
                ->withErrors($offer->getErrors());
        }
    }
}
