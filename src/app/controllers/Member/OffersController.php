<?php

namespace Member;

// Domain
use Offer;
use Confide;

// Core
use Input;
use Redirect;
use Request;
use View;

// Third Party
use Notification;

class OffersController extends \BaseController
{
    /**
     * Display listing of raw offers.
     *
     * @return Response
     */
    public function index()
    {
        $offers = \Collection::make([
            'pending' => Confide::user()->offersMade()->pending()->get(),
            'actioned' => Confide::user()->offersMade()->actioned()->get(),
        ]);
        return View::make('member.offers.index', compact('offers'));
    }


    /**
     * Return a subset of pending offers for use in a dashboard Ajax call
     *
     * @return Response
     */
    public function allAwaitingResponse()
    {
        if (Request::ajax()) {
            $offers = Confide::user()->offersMade()->pending()->forAjax()->get();
            $view = $offers->isEmpty() ? 'member.dashboard.no-data' : 'member.offers._table';
            return View::make($view, compact('offers'));
        } else {
            Route::redirect('member.offers.index');
        }
    }


    /**
     * Return a subset of actioned offers for use in a dashboard Ajax call
     *
     * @return Response
     */
    public function allActioned()
    {
        if (Request::ajax()) {
            $offers = Confide::user()->offersMade()->actioned()->forAjax()->get();
            $view = $offers->isEmpty() ? 'member.dashboard.no-data' : 'member.offers._table';
            return View::make($view, compact('offers'));
        } else {
            Route::redirect('member.offers.index');
        }
    }


    /**
     * Display listing of purchases.
     *
     * @return Response
     */
    public function purchases()
    {
        $offers = \Collection::make([
            'awaiting_payment' => Confide::user()->offersMade()->paymentRequired()->get(),
            'purchase_history' => Confide::user()->offersMade()->paymentComplete()->get(),
        ]);
        return View::make('member.offers.purchases', compact('offers'));
    }


    /**
     * Display a listing of offers converted to purchases (i.e. accepted) and awaiting
     * payment, or a subset for Ajax requests
     *
     * @return Response
     */
    public function paymentRequired()
    {

        if (Request::ajax()) {
            $payment_required = Confide::user()->offersMade()->paymentRequired()->forAjax()->get();
            $view = $payment_required->isEmpty() ? 'member.dashboard.no-data' : 'member.offers.payment-required._table';
            return View::make($view, compact('payment_required'));
        } else {
            $payment_required = Confide::user()->offersMade()->paymentRequired()->get();
            return View::make('member.offers.payment-required.index', compact('payment_required'));
        }
    }


    /**
     * Display a history of purchased items, or a subset for Ajax requests
     *
     * @return Response
     */
    public function purchaseHistory()
    {
        if (Request::ajax()) {
            $purchases = Confide::user()->offersMade()->paymentComplete()->forAjax()->get();
            $view = $purchases->isEmpty() ? 'member.dashboard.no-data' : 'member.offers.history._table';
            return View::make($view, compact('purchases'));
        } else {
            $purchases = Confide::user()->offersMade()->paymentComplete()->get();
            return View::make('member.offers.history.index', compact('purchases'));
        }
        
    }


    /**
     * Mark an offer as accepted, after checking parent advert owned by current user
     *
     * @param  Offer $offer
     * @return Response
     */
    public function accept($offer)
    {
        if ($offer->advert->owner->id == Confide::user()->id 
            && $offer->fill(['acceptance_status' => Offer::OFFER_ACCEPTED])->save()
        ) {
            Notification::success('Offer accepted!');
        } else {
            Notification::error('Access denied!');
        }
        return Redirect::back();
    }


    /**
     * Mark an offer as declined, after checking parent advert owned by current user
     *
     * @param  Offer $offer
     * @return Response
     */
    public function decline($offer)
    {
        if ($offer->advert->owner->id == Confide::user()->id
            && $offer->fill(['acceptance_status' => Offer::OFFER_DECLINED])->save()
        ) {
            Notification::info('Offer declined!');
        } else {
            Notification::error('Access denied!');
        }

        return Redirect::back();
    }
}
