<?php

namespace Member;

// Domain
use Advert;
use Category;
use Manufacturer;
use Country;
use PostageOption;
use Confide;
use Offer;

// Core
use Input;
use Redirect;
use View;
use Session;
use URL;
use Request;
use Response;
use Carbon\Carbon;

// Third Party
use Notification;

class AdvertsController extends \BaseController
{

    /**
     * Register filters
     */
    public function __construct()
    {
        $this->beforeFilter('extended_profile_holder', ['except' => 'index', 'delete']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $adverts = \Collection::make([
            'not_completed' => Advert::ownedBySelf()->notCompleted()->get(),
            'completed' => Advert::ownedBySelf()->completed()->get(),
        ]);
        return View::make('member.listings.index', compact('adverts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $advert = new Advert();
        $rules = $advert->getRules();
        $categories = Category::getFullHierachyNestedList();
        $manufacturers = Manufacturer::lists('name', 'id');

        return View::make(
            'member.listings.create',
            compact('advert', 'rules', 'categories', 'manufacturers')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        return $this->handleSave(new Advert);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Advert  $advert
     * @return Response
     */
    public function edit(Advert $advert)
    {
        $rules = $advert->getRules();
        $categories = Category::getFullHierachyNestedList();
        $manufacturers = Manufacturer::lists('name', 'id');

        return View::make(
            'member.listings.edit',
            compact('advert', 'rules', 'categories', 'manufacturers')
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Advert  $advert
     * @return Response
     */
    public function update(Advert $advert)
    {
        return $this->handleSave($advert);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Advert  $advert
     * @return Response
     */
    public function destroy(Advert $advert)
    {
        $advert->delete();
        return Redirect::route('member.listings.index');
    }


    /**
     * Called from the store and update methods to validate/save/redirect
     * @param  Advert $advert
     * @return Response
     */
    private function handleSave($advert)
    {
        // Fill the $advert (excluding non member editable fields),
        $advert->fill(
            Input::except(
                'user_id',
                'approval_status',
                'purchased_at',
                'reserved_at',
                'shipped_at'
            )
        );

        // Set the user manually
        $advert->user_id = Confide::user()->id;

        if ($advert->save()) {
            Notification::success('Advert saved');
            return Redirect::route('member.listings.interstitial', $advert->id);
        } else {
            return Redirect::back()->withErrors($advert->getErrors())->withInput();
        }
    }


    ///////////////////
    // Extra methods //
    ///////////////////


    /**
     * Display a cut down preview version of the frontend page
     * 
     * @param  Advert $advert
     * @return Response
     */
    public function preview(Advert $advert)
    {
        return View::make('public.advert.preview', compact('advert'));
    }


    /**
     * Display full summary details of the resource
     *
     * @param  Advert $advert
     * @return Response
     */
    public function summary(Advert $advert)
    {
        return View::make('member.listings.summary', compact('advert'));
    }


    /**
     * Present some "what next" options to the member, normally advert is draft at this point
     *
     * @param  Advert $advert
     * @return Response
     */
    public function interstitial(Advert $advert)
    {
        return View::make('member.listings.interstitial', compact('advert'));
    }


    /**
     * Send completed listing to admins to be approved
     * @param  Advert $advert
     * @return Response
     */
    public function submitForApproval(Advert $advert)
    {
        $advert->makeDraft();
        $advert->submitForApproval();
        return View::make('member.listings.finished', compact('advert'));
    }


    /**
     * Show all active selling items (or a subset for Ajax requests)
     *
     * @return Response
     */
    public function allActiveSelling()
    {
        if (Request::ajax()) {
            $adverts = Advert::ownedBySelf()->notCompleted()->forAjax()->get();
            $view = $adverts->isEmpty() ? 'member.dashboard.no-data' : 'member.listings._table';
            return View::make($view, compact('adverts'));
        } else {
            $adverts = Advert::ownedBySelf()->notCompleted()->get();
            return View::make('member.listings.active', compact('adverts'));
        }
    }


    /**
     * Show all recently sold items (or a subset for Ajax requests)
     *
     * @return Response
     */
    public function recentlySold()
    {
        if (Request::ajax()) {
            $adverts = Advert::ownedBySelf()->completed()->recentlyModified()->forAjax()->get();
            $view = $adverts->isEmpty() ? 'member.dashboard.no-data' : 'member.listings._table';
            return View::make($view, compact('adverts'));
        } else {
            $adverts = Advert::ownedBySelf()->completed()->recentlyModified()->get();
            return View::make('member.listings.recently-sold', compact('adverts'));
        }
    }


    /**
     * Show an edit form for managing postage options
     *
     * @param  Advert $advert
     * @return Response
     */
    public function editPostage(Advert $advert)
    {
        // Set up Redirect::intended() for use during a cancel click
        if (URL::current() != URL::previous()) {
            Session::put('url.intended', URL::previous());
        }

        $regions = new \Collection;
        
        // Get all regions and add a placeholder PostageOption model
        foreach (Country::getRegions() as $region_name) {
            $placeholder = new PostageOption();
            $regions->put($region_name, $placeholder);
        }

        // Iterate existing PostageOptions and replace the placeholders
        foreach ($advert->postage as $postage_option) {
            $postage_option->enabled = true;
            $regions->put($postage_option->region_name, $postage_option);
        }

        return View::make('member.listings.postage', compact('advert', 'regions'));
    }


    /**
     * Save postage updates
     *
     * @param  Advert $advert
     * @return Response
     */
    public function updatePostage(Advert $advert)
    {
        // Build a new model for each delivery option from the form
        $postage_options = new \Collection;
        foreach (Input::get('postage_options') as $form_input) {
            if (isset($form_input['enabled'])) {
                $model = new PostageOption;

                // Fill the remaining date and roundtrip the checkbox
                $model->fill($form_input + ['enabled' => true]);

                // Collect in person is a special case - ensure it has not been tempered with
                if ($form_input['region_name'] == PostageOption::COLLECT_IN_PERSON) {
                    $model->fill(PostageOption::$collect_in_person_prototype);
                }
                
                // Add to the options collection
                $postage_options->push($model);
            }
        }
        
        // Ensure at least one option was selected
        if ($postage_options->isEmpty()) {
            Notification::error('Please complete at least one postage option');
            return Redirect::back()->withInput();
        }

        // Check each is valid, collect up errors if any fail
        $postage_errors = new \Collection;
        foreach ($postage_options as $postage_option) {
            if ($postage_option->isInvalid()) {
                $postage_errors->put($postage_option->region_name, $postage_option->getErrors());
            }
        }

        // Flash errors and return to form if any errors occurred
        if (! $postage_errors->isEmpty()) {
            \Session::flash('postage_errors', $postage_errors);
            return Redirect::back()->withInput();
        }

        // No errors, clear out old shipping and save new
        $advert->postage()->delete();
        $advert->postage()->saveMany($postage_options->all());

        // Return
        Notification::success('Postage options have been updated');
        return Redirect::route('member.listings.interstitial', ['id' => $advert->id]);
    }


    /**
     * Set shipped status
     *
     * @param  Advert $advert
     * @return Response
     */
    public function shipped(Advert $advert)
    {
        if ($advert->markShipped()) {
            Notification::success('Shipping status has been updated');
        } else {
            Notification::error('Shipping status cannot be updated');
        }

        return Redirect::back();
    }


    /**
     * Cancel a non-paid for sale and return to listings
     *
     * @param  Advert $advert
     * @return Response
     */
    public function cancel(Advert $advert)
    {
        if ($advert->cancelPendingSale()) {
            Notification::success('Reservation has been cancelled and the item returned to sale');
            return Redirect::back();
        } else {
            Notification::error('This item cannot be returned to sale');
            return Redirect::back();
        }
    }
}
