<?php

namespace Admin;

// Domain
use Activity;
use Role;

// Core
use Input;
use Redirect;
use View;

// Third Party
use Notification;
use Confide;

class ActivitiesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /tags
     *
     * @return Response
     */
    public function index()
    {
        $activities = Role::where('name', '=', Role::ROLE_ADMIN)->firstOrFail()->activities;
        return View::make('admin.activities.index', compact('activities'));
    }


    /**
     * Mark as read - called via ajax
     *
     * @param  Activity $activity
     * @return Response
     */
    public function markRead(Activity $activity)
    {
        $activity->read = true;
        $activity->save();
        return Response::json('Marked as read', 200);
    }


    /**
     * Update the resource (simply toggle read/unread status)
     *
     * @param  Activity $activity
     * @return Response
     */
    public function update(Activity $activity)
    {
        $activity->read = $activity->read ? false : true;
        $activity->save();
        Notification::success('Activity read status toggled!');
        return Redirect::route('admin.activities.index');
    }


    /**
     * Update all resources for this role (simply mark them as read)
     *
     * @return Response
     */
    public function updateAll()
    {
        Role::where('name', '=', Role::ROLE_ADMIN)->firstOrFail()->activities()
            ->update(['read' => true]);
        Notification::success('Marked all activities as read!');
        return Redirect::route('admin.activities.index');
    }


    /**
     * Delete the resource
     *
     * @param  Activity $activity
     * @return Response
     */
    public function destroy(Activity $activity)
    {
        $activity->delete();
        Notification::success('Activity deleted!');
        return Redirect::route('admin.activities.index');
    }


    /**
     * Delete all resources for this user
     *
     * @return Response
     */
    public function destroyAll()
    {
        Role::where('name', '=', Role::ROLE_ADMIN)->firstOrFail()->activities()
            ->delete();
        Notification::success('All activities deleted');
        return Redirect::route('admin.activities.index');
    }
}