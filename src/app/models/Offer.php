<?php

// Core
use Carbon\Carbon;

class Offer extends Eloquent
{

    use SoftDeletingTrait;

    protected $fillable = ['offer_price', 'acceptance_status'];

    protected $dates = ['expires_at'];

    // Special case - keep the rules in a static to allow access externally
    // without instantiating the class, populate $this->rules from constructor
    public static $static_rules = ['offer_price' => ['required', 'numeric']];
    protected $rules = [];

    // Def class constants and a container to reference the acceptance status
    const OFFER_PENDING = 00;
    const OFFER_DECLINED = 10;
    const OFFER_DECLINED_AUTO = 11;
    const OFFER_ACCEPTED = 20;
    const OFFER_SELLER_CANCELLED = 30;
    public static $acceptance_statuses = [
        self::OFFER_PENDING => 'Pending',
        self::OFFER_DECLINED => 'Declined',
        self::OFFER_DECLINED_AUTO => 'Declined',
        self::OFFER_ACCEPTED => 'Accepted',
        self::OFFER_SELLER_CANCELLED => 'Cancelled by seller',
    ];

    ///////////////
    // Bootstrap //
    ///////////////


    public function __construct()
    {
        parent::__construct();

        // Set up rules
        $this->rules = self::$static_rules;
    }


    /**
     * Boot the parent, register observer
     */
    public static function boot()
    {
        parent::boot();

        Offer::observe(new OfferObserver);
    }


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * BelongsTo relationship with source advert
     *
     * @return Advert
     */
    public function advert()
    {
        return $this->belongsTo('Advert');
    }


    /**
     * BelongsTo relationship with potentially buying user
     *
     * @return User
     */
    public function initiator()
    {
        return $this->belongsTo('User', 'initiator_id');
    }


    ///////////////
    // Accessors //
    ///////////////


    /**
     * Virtual property for display of offer acceptance status
     *
     * @return string
     */
    public function getAcceptanceStatusStringAttribute()
    {
        return self::$acceptance_statuses[$this->acceptance_status];
    }


    /**
     * Get a GBP formatted currency string for the offer price
     *
     * @return String
     */
    public function getGbpOfferPriceStringAttribute()
    {
        return Converter::to('currency.gbp')->value($this->offer_price)->format();
    }


    ////////////
    // Scopes //
    ////////////


    /**
     * Limit query to only pending (not expired, declined or auto declined).
     * NOTE: extra where closure for query grouping
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopePending($query)
    {
        return $query->where(function($query) {
            $query->where('expires_at', '>=', Carbon::now())
            ->where('acceptance_status', '=', self::OFFER_PENDING);
        });
    }


    /**
     * Limit query to only accepted offers with outstanding payment
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopePaymentRequired($query)
    {
        return $query->where(function($query) {
            $query->where('acceptance_status', '=', self::OFFER_ACCEPTED)
            ->whereHas('advert', function($query) {
                $query->whereNull('purchased_at');
            });
        });
    }


    /**
     * Limit query to only accepted offers which have been paid for
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopePaymentComplete($query)
    {
        return $query->where(function($query) {
            $query->where('acceptance_status', '=', self::OFFER_ACCEPTED)
            ->whereHas('advert', function($query) {
                $query->whereNotNull('purchased_at');
            });
        });
    }


    /**
     * Limit query to only actioned (expired or not on pending status)
     * NOTE: extra where closure for query grouping
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeActioned($query)
    {
        return $query->where(function($query) {
            $query->where('expires_at', '<', Carbon::now())
            ->orWhere('acceptance_status', '!=', self::OFFER_PENDING);
        });
    }


    /**
     * Scope to limit results for use in Ajax partials
     *
     * @param Query $query
     * @return Query
     */
    public function scopeForAjax($query)
    {
        return $query->orderBy('created_at', 'DESC')->limit(10);
    }
}
