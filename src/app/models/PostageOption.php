<?php

use Watson\Validating\ValidatingTrait;

class PostageOption extends Eloquent
{
    use ValidatingTrait;

    protected $fillable = ['region_name', 'description', 'price'];

    protected $rules = null; // Set the rules in constructor

    // Constant to deal with the 'collect in person' special case
    const COLLECT_IN_PERSON = 'Collect in person';
    public static $collect_in_person_prototype = [
        'description' => 'Collect directly from the seller in person',
        'price' => 0,
    ];

    public function __construct()
    {
        parent::__construct();

        $min_price = Config::get('domain.postage.min_price', 0);
        $max_price = Config::get('domain.postage.max_price', 999.99);

        $this->rules = [
            'region_name' => [
                'required',
                'max:255',
                'in:'. implode(',', Country::getRegions()),
            ],
            'description' => [
                'required',
                'max:255',
            ],
            'price' => [
                'required',
                'numeric',
                "between:{$min_price},{$max_price}"
            ],
        ];
    }


    ///////////////
    // Accessors //
    ///////////////


    /**
     * Get a GBP formatted currency string for the price
     *
     * @return String
     */
    public function getGbpPriceStringAttribute()
    {
        if ($this->price == 0) {
            return 'Free of charge';
        } else {
            return Converter::to('currency.gbp')->value($this->price)->format();
        }
    }


    /**
     * Get an array for use in a dropdown
     *
     * @return array
     */
    public function getDisplayStringAttribute()
    {
        return "{$this->region_name}: $this->gbp_price_string";
    }
}
