<?php

class Activity extends Eloquent
{
    use SoftDeletingTrait;

    protected $fillable = ['title', 'body'];


    ///////////////////
    // Relationships //
    ///////////////////
    

    /**
     * Define belongsTo relationship with user
     *
     * @return Collection
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * Define belongsTo relationship with role
     *
     * @return Collection
     */
    public function role()
    {
        return $this->belongsTo('Role');
    }


    ///////////////
    // Accessors //
    ///////////////


    /**
     * Get a short, tagless version of the body content
     *
     * @return string
     */
    public function getBodySummaryAttribute()
    {
        return substr(strip_tags($this->body), 0, 100);
    }


    ////////////
    // Scopes //
    ////////////


    /**
     * Filter results to only activities owned by current user
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeOwnedBySelf($query)
    {
        if (Confide::user()) {
            return $query->where('user_id', '=', Confide::user()->id);
        } else {
            // If not logged in ensure the scope returns nothing
            return $query->whereNull('id');
        }
    }


    /**
     * Filter to include unread only
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeUnread($query)
    {
        return $query->whereNull('read');
    }
}
