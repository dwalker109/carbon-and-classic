<?php

use Watson\Validating\ValidatingTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Profile extends Eloquent implements StaplerableInterface
{

    use ValidatingTrait;

    use EloquentTrait;

    protected $fillable = [
        'user_id', // Needed for firstOrNew() to work, be careful with form input!
        'paypal_id',
        'full_name',
        'address_line_1',
        'address_line_2',
        'address_line_3',
        'city',
        'postal_code',
        'telephone',
        'telephone_country',
        'avatar',
    ];

    protected $rules = [
        'paypal_id' => ['max:255', 'email'],
        'full_name' => ['required', 'max:255', 'alpha_spaces'],
        'address_line_1' => ['required', 'max:255', 'alpha_spaces'],
        'address_line_2' => ['max:255', 'alpha_spaces'],
        'address_line_3' => ['max:255', 'alpha_spaces'],
        'city' => ['required', 'max:100', 'alpha_spaces'],
        'postal_code' => ['required', 'max:20', 'alpha_spaces'],
        'telephone' => ['phone'],
        'avatar' => ['stapler_image'],
    ];

    public function __construct(array $attributes = array())
    {
        // Add an additional dynamically generated validation rule
        $this->rules['telephone_country'] = [
            'required_with:telephone',
            'max:20',
            'in:'. implode(',', array_keys(Country::getList())),
        ];

        // Setup Stapler image
        $this->hasAttachedFile('avatar', [
            'styles' => [
                'large' => '600x600',
                'medium' => '300x300',
                'thumb' => '100x100',
            ]
        ]);

        parent::__construct($attributes);
    }


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * Define relationship to User
     *
     * @return  User
     */
    public function user()
    {
        return $this->belongsTo('User');
    }


    /**
     * Define hasOne relationship with countries
     *
     * @return Country
     */
    public function country()
    {
        return $this->hasOne('Country', 'alpha_2_code', 'telephone_country');
    }

    ///////////////
    // Accessors //
    ///////////////


    /**
     * Return all non-empty address components in an array
     *
     * @return array
     */
    public function getAddressComponentsAttribute()
    {
        $address_components = [
            'address_line_1' => $this->address_line_1,
            'address_line_2' => $this->address_line_2,
            'address_line_3' => $this->address_line_3,
            'city' => $this->city,
            'telephone_country' => $this->telephone_country,
            'postal_code' => $this->postal_code,
            'telephone' => $this->telephone,
        ];
        return array_where($address_components, function($key, $value) {
            return (! empty($value));
        });
    }


    //////////
    // Misc //
    //////////


    /**
     * Static call to return a bare-bones mock of a Profile, for those rare
     * occasions where a Profile should exists but doesn't
     *
     * @return Profile
     */
    public static function makeDummy()
    {
        $profile = new Profile();
        $profile->fill(Config::get('domain.profile.dummy'));
        return $profile;
    }
}
