<?php

use Watson\Validating\ValidatingTrait;

class Manufacturer extends Eloquent
{

    // Const to use when building searchfilters
    const SFKEY = 'searchfilter_manufacturer_';

    use ValidatingTrait;

    use SoftDeletingTrait;

    protected $fillable = ['name', 'description', 'active'];

    protected $rules = [
        'name' => ['required', 'max:255'],
        'description' => ['max:255'],
        'active' => ['integer'],
    ];


    ///////////////////
    // Relationships //
    ///////////////////


    /**
     * Define hasMany relationship with adverts
     *
     * @return Collection
     */
    public function adverts()
    {
        return $this->hasMany('Advert');
    }
}
