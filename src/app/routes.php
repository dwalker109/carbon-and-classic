<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Routes are split into seperate files - get them here
foreach (File::allFiles(__DIR__ . '/routes') as $files) {

    require_once $files->getPathName();

}
