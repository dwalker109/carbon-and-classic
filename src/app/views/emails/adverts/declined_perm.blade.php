<h1>Sorry {{ $profile->full_name }}, your advert has been declined</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->gbp_price_string }}</p>

<p>Afer reviewing your advert we have decided that it is not suitable for display on the
site, and will shortly be deleted.</p>

<p>You may receive feeback on why we have taken this decision at a later date.</p>

<p>Please do not re-list this item without first discusing it with us.</p>
