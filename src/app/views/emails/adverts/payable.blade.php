<h1>Congratulations, your offer has been accepted and payment is now due</h1>

<h2>Summary</h2>
<p>Title: {{ $advert->title }}</p>
<p>Price: {{ $advert->winning_offer->gbp_offer_price_string }}</p>

<p>Please {{ link_to_route('member.offers.payment-required', 'complete your payment') }}
within 3 working days.</p>

<ul>
	<li>If you have any issues, please contact the seller in the first instance.</li>
	<li>If the seller is not able to resolve your issues, please get in touch with us directly.</li>
</ul>

<p>Thanks for using Carbon and Classic!</p>