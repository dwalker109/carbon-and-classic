<div id="category-inner-wrapper">

	{{-- Main pane --}}
	<section id="category-main" class="category-main category-main-filters-hidden">

		{{-- Content pulled from seperate view so it can be refreshed via Ajax later --}}
		@include('public.listings._main-content')

	</section>{{-- ./category-main --}}

</div>{{-- ./category-inner-wrapper --}}