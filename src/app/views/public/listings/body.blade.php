@extends('public.layout')

@section('title', $category->name)

@section('content')

@include($category->is_advert_container 
	? 'public.listings._advert-container' 
	: 'public.listings._subcategory-container'
)

@stop
