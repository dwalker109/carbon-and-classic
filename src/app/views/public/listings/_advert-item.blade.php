{{-- Link, and nested divs containing item --}}
<a href="{{ route('public.advert', [$advert->slug]) }}">

	<div class="listing-item listing-item-filters-hidden equal-height">
		<div class="listing-item-container">

			<img src="{{ $advert->primary_photo->image->url('large') }}" />
			
			<div class="listing-item-name-and-location">
				<p>{{ $advert->title }}</p>
				<p>{{ $advert->location_string }}</p>
			</div>

			<div class="listing-item-price">
				<p>{{ $advert->gbp_price_string }}</p>
			</div>

		</div> {{-- ./listing-item-container --}}
	</div> {{-- ./listing-item --}}

</a>
