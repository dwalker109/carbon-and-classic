{{-- Link, and nested divs containing item --}}
<a href="{{ route('public.category', [$subcategory->slug]) }}">

	<div class="category-item">
		<div class="category-item-container equal-height">

			<img src="{{ $subcategory->image->url('large') }}">
			<div class="category-name">
				<p>{{ $subcategory->name }}</p>
			</div>

		</div> {{-- ./category-item-container --}}
	</div> {{-- ./category-item --}}
</a>
