@extends('public.layout')

@section('title', $cms_page->title)

@section('meta_description', $cms_page->meta_description)

@section('content')

<div class="page-wrapper">

	<section class="page-body">

		{{ $cms_page->body }}

	</section>

	<section class="cms_page-images">

		@unless ($cms_page->photos->isEmpty())

			{{-- Build full images --}}
			<ul class="bxslider">
				@foreach ($cms_page->photos as $photo)
					<li><img src="{{ $photo->image->url('large') }}" /></li>
				@endforeach
			</ul>

			{{-- Build thumbnail pager --}}
			<div id="bx-pager">
				@foreach ($cms_page->photos as $n => $photo)
					<a data-slide-index="{{ $n }}" href="#"><img src="{{ $photo->image->url('thumb') }}" /></a>
				@endforeach
			</div>

		@endunless

	</section>{{-- ./cms_page-images --}}

</div>

@stop
