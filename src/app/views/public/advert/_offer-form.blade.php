{{-- Partial used as a bootstrop popover - nake sure to only use double quotes ("")
in this file, single quotes will break the calling htmk element!!! --}}

<p>By making an offer on this item you agree that if your offer is accepted, 
you will complete a payment, via PayPal, within three days. 
If you have questions about the item you should message the seller before 
making an offer.</p>

{{ Former::withRules($rules) }}
{{ Former::open(URL::route('offer.make', $advert->slug)) }}
<div class="input-group">
	<input type="text" name="offer_price" class="form-control" placeholder="£">
    <span class="input-group-btn">
		<input type="submit" class="btn btn-default" value="OK">
    </span> {{-- ./input-class-btn --}}
</div> {{-- ./input-group --}}
{{ Former::close() }}