@extends('admin.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">
		
		<h1>Categories</h1>

		<div class="alert alert-info" role="alert">
			<p>Deleting categories will <strong>not</strong> remove existing 
			adverts they contain and the adverts <strong>will</strong> still 
			appear in search results, but the category <strong>will not</strong>
			be shown when drilling down through the site, and will 
			<strong>not</strong> be available for any new adverts.</p>
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">
				<a class="btn btn-default" href="{{ route('admin.categories.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New Category</a>
			</div>

			<div class="panel-body">

				@if ($categories->isEmpty())

					<div class="alert alert-warning" role="alert">
						<p>No categories found</p>
					</div>

				@else 

					<table class="table table-striped data-table" id="admin-categories-index">
						<thead>
							<tr>
								<th data-orderable="false">{{-- Manage buttons --}}</th>
								<th>{{-- Chain --}}</th>
							</tr>
						</thead>
						<tbody>
						@foreach ($categories as $category)
							<tr>
								<td>

									{{-- Manage dropdown --}}
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-label="Manage Item">
											<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li>
												{{ Form::open(array('route' => array('admin.categories.edit', $category->id), 'method' => 'GET')) }}
												    <button type="submit" class="btn btn-link">Edit</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.categories.destroy', $category->id), 'method' => 'DELETE')) }}
												    <button type="submit" class="btn btn-link">Delete</button>
												{{ Form::close() }}
											</li>
										</ul>
									</div> {{--- ./Manage dropdown --}}

								</td>

								{{-- Output category ancestor chain and own name --}}
								<td>
									@foreach ($category->getAncestors() as $ancestor)
										{{ $ancestor->name }} &rArr;
									@endforeach
									<strong>{{ $category->name }}</strong>
								</td>
								
							</tr>
						@endforeach
						</tbody>
					</table>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop