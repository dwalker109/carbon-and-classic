@extends('admin.categories._form')

@section('main')

<div class="panel-heading">
	<h1>Add Category</h1>
</div>

<div class="panel-body">

	{{ Former::open_for_files(route('admin.categories.store'))->method('POST') }}

		@include('admin.categories._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop