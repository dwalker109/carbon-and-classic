@extends('public.layout-popup')

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="well">
			<p>This is a preview of the advert, as it will appear on the site.
			All postage options are being displayed (logged in users will only see the
			ones which apply to them).</p>
			<p class="text-warning">If you have made any changes to the advert, you must save
			them before they will be reflected in this preview.</p>
		</div>
	</div>
</div>

<?php $preview_mode = true ?>

<div class="listing-preview-container">

	<div class="listing-wrapper">

		{{-- Include the main advert content --}}
		@include('public.advert._body_with_images')

	</div>

</div>

@stop
