@extends('admin.adverts._form')

@section('main')

<div class="panel-heading">
	<h1>Edit Advert</h1>
	{{ link_to_route(
		'admin.adverts.preview', 
		'Show preview, including image gallery and postage options', 
		$advert->id, 
		['class' => 'btn btn-default show-listing-preview-modal']) 
	}}
</div>

<div class="panel-body">

	{{ Former::open(route('admin.adverts.update', $advert->id))->method('PUT') }}

		@include('admin.adverts._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop
