<header class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
			</button>
		</div>
		<nav class="collapse navbar-collapse bs-navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Manage <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li>{{ link_to_route('admin.manufacturers.index', 'Manufacturers') }}</li>
						<li>{{ link_to_route('admin.categories.index', 'Categories') }}</li>
						<li class="divider"></li>
						<li>{{ link_to_route('admin.members.index', 'Members') }}</li>
						<li>{{ link_to_route('admin.adverts.index', 'Adverts') }}</li>
						<li class="divider"></li>
						<li>{{ link_to_route('admin.cms-pages.index', 'CMS Pages') }}</li>
					</ul>
				</li>
				<li><a href="{{ URL::route('messages') }}">Messages <span class="badge">{{ Confide::user()->newMessagesCount() }}</span></a></li>
				<li><a href="{{ URL::route('admin.activities.index') }}">Alerts <span class="badge">{{ Role::where('name', '=', Role::ROLE_ADMIN)->firstOrFail()->activities()->unread()->count() }}</span></a>
				</li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a>{{ Confide::user()->email }}</a></li>
				<li><a href="{{ URL::route('logout') }}">Logout <span class="glyphicon glyphicon-log-out"></span></a></li>
			</ul>
		</nav>
	</div>
</header>