@extends('admin.manufacturers._form')

@section('main')

<div class="panel-heading">
	<h1>Edit Manufacturers</h1>
</div>

<div class="panel-body">

	{{ Former::open(route('admin.manufacturers.update', $manufacturer->id))->method('PUT') }}

		@include('admin.manufacturers._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop