{{ Former::populate($manufacturer) }}
{{ Former::withRules($rules) }}

{{ Former::text('name') }}
{{ Former::textarea('description')->rows(3) }}

{{ Former::actions(
	Button::primary('Save')->submit(),
	Button::danger('Cancel')->asLinkTo(route('admin.manufacturers.index'))
) }}