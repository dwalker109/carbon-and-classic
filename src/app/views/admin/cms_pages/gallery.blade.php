@extends('admin.cms_pages._form')

@section('main')

<div class="panel-heading">
	<h1>Manage Gallery</h1>
</div>

<div class="panel-body">

	<div class="row">

		<div class="col-sm-6">

			<h2>Upload pictures</h2>

			<form action="{{ route('admin.cms-pages.gallery.store', [$cms_page->id]) }}"
				class="dropzone" id="gallery-dropzone">
				<input type="hidden" name="sequence" value="255" />
				<div class="dz-message">
				    Drag and drop files here, or click to upload. You can upload
				    multiple files at the same time!</span>
				</div>
			</form>

			<div class="gallery-finish-boxout">

				<h3>Finished uploading?</h3>

				<p>
					<a href="{{ route('admin.cms-pages.index') }}" 
					class="btn btn-primary">Yes, return</a>
				</p>

			</div>

		</div>

		<div class="col-sm-6">

			<h2>Existing pictures</h2>

			<div class="alert alert-info" role="alert">
				<p>The <strong>first</strong> image listed will be used in 
				certain lists. Just drag and drop to re-order.</p>
			</div>

			@if ($cms_page->photos)

				<div id="gallery" class="sortable" 
					data-save-sort-url="{{ route('admin.cms-pages.gallery.save_sort', [$cms_page->id]) }}"
					data-fetch-url="{{ route('admin.cms-pages.gallery.fetch', [$cms_page->id, null]) }}">

					@foreach ($cms_page->photos as $photo)

						@include('admin.cms_pages._galleryitem', compact('advert', 'photo'))
					
					@endforeach

				</div>

			@endif

		</div>

	</div> {{-- /.Main row --}}

</div> {{-- /.panel-body --}}

@stop
