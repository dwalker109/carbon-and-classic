@extends('admin.layout')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">
		
		<h1>Alerts</h1>

		<div class="alert alert-info" role="alert">
			<p>This list provides an at-a-glace view of various alerts which may require your
			attention. This listing is shared by all admins, so actions you take here affect any other admins
			as well.</p>
		</div>

		<div class="panel panel-primary">

			<div class="panel-heading">
				<a class="btn btn-default" href="{{ route('admin.activities.update_all') }}" role="button"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Mark all as read</a>
				<a class="btn btn-default" href="{{ route('admin.activities.delete_all') }}" role="button"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete all</a>

			</div>

			<div class="panel-body">

				@if ($activities->isEmpty())

					<div class="alert alert-warning" role="alert">
						<p>No alerts found. Once read, alerts are removed after one month.</p>
					</div>

				@else 

					<table class="table table-striped data-table-plain" id="admin-activities-index">

						<thead>
							<tr>
								<th>{{-- Read status --}}</th>
								<th>{{-- Controls --}}</th>
								<th>{{-- Added --}}</th>
								<th>{{-- Title --}}</th>
								<th>{{-- Body --}}</th>
							</tr>
						</thead>

						<tbody>

						@foreach ($activities as $activity)
						
							<tr>

								<td>
									{{-- Read/unread marks --}}
									@if (!$activity->read)
										<?php $glyphicon = 'glyphicon-certificate' ?>
									@else
										<?php $glyphicon = 'glyphicon-ok-circle' ?>
									@endif
									<span class="glyphicon {{ $glyphicon }}" aria-hidden="true"></span>
								</td>

								<td>
									{{-- Manage dropdown --}}
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-label="Manage Item">
											<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li>
												{{ Form::open(array('route' => array('admin.activities.update', $activity->id), 'method' => 'PATCH')) }}
												    <button type="submit" class="btn btn-link">Toggle read/unread</button>
												{{ Form::close() }}
											</li>
											<li>
												{{ Form::open(array('route' => array('admin.activities.destroy', $activity->id), 'method' => 'DELETE')) }}
												    <button type="submit" class="btn btn-link">Delete</button>
												{{ Form::close() }}
											</li>
										</ul>
									</div> {{--- ./Manage dropdown --}}
								</td>

								{{-- Output the rest of the line data --}}
								<td>{{ $activity->created_at->diffForHumans() }}</td>
								<td><strong>{{ $activity->title }}</strong></td>
								<td>
									{{-- Show body summary and a link to pass to bootbox.js --}}
									{{ $activity->body_summary }}&hellip;
									<a href="#" class="bootbox-alerts-popup" data-message="{{{ $activity->body }}}"
									data-read-url="{{ route('member.activities.mark_read', ['id' => $activity->id]) }}">View more</a>
								</td>

							</tr>

						@endforeach

						</tbody>
					</table>

				@endif

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop