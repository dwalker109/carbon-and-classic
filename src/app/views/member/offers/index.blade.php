@extends('member.layout')

@section('title', 'All Offers')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>All Offers</h1>
			</div>

			<div class="panel-body">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab">Awaiting a response from Seller</a></li>
					<li role="presentation"><a href="#actioned" aria-controls="actioned" role="tab" data-toggle="tab">Accepted or declined by Seller</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">

					<div role="tabpanel" class="tab-pane fade in active" id="pending">

						@if ($offers->get('pending')->isEmpty())

							<div class="well">
								<p>You don't have any outstanding offers to display.</p>
							</div>

						@else

							@include('member.offers._table', ['offers' => $offers->get('pending')])

						@endif
						
					</div>{{-- ./tab-pane --}}
					
					<div role="tabpanel" class="tab-pane fade" id="actioned">

						@if ($offers->get('actioned')->isEmpty())

							<div class="well">
								<p>You don't have any outstanding offers to display.</p>
							</div>

						@else

							@include('member.offers._table', ['offers' => $offers->get('actioned')])

						@endif

					</div>{{-- ./tab-pane --}}
				
				</div>{{-- ./tab-content --}}

				{{ link_to_route('member.dashboard', 'Return to Dashboard', null, ['class' => 'btn btn-primary']) }}

			</div> {{-- /.panel-body --}}
		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
