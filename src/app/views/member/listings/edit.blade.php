@extends('member.listings._form')

@section('title', "Editing {$advert->title}")

@section('main')

<div class="panel-heading">
	<h1>Edit Listing</h1>
	<p class="text-info">Please note that once saved, your advert may be temporarily 
	unavailable while we approve your updates.</p>
	<p class="text-info">Editing your listing's <b>price</b> or <b>minimum offer price</b>
	will not return it to draft status, so feel free to amend these whenever you need to.</p>
</div>

<div class="panel-body">

	{{ Former::open(route('member.listings.update', $advert->id))->method('PUT') }}

		@include('member.listings._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop
