@extends('member.listings._form')

@section('title', "Adding a new listing")

@section('main')

<div class="panel-heading">
	<h1>Add Listing</h1>
</div>

<div class="panel-body">

	{{ Former::open(route('member.listings.store'))->method('POST') }}

		@include('member.listings._fields')

	{{ Former::close() }}

</div> {{-- /.panel-body --}}

@stop
