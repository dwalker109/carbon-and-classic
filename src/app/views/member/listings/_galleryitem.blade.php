{{-- Partial used to render a gallery item --}}

<div data-id="{{ $photo->id }}" class="sortable-item gallery-item well well-sm clearfix">
	
	<div class="row">

		<div class="col-xs-8">

			<p>Uploaded: <span class="text-muted">{{ $photo->image_updated_at->diffForHumans() }}</span></p>
			{{ Form::open(['url' => route('member.listings.gallery.destroy', [$advert->id, $photo->id]), 'method' => 'DELETE']) }}
			    <button type="submit" class="btn btn-xs btn-danger gallery-delete">
			    	<span class="glyphicon glyphicon-remove"></span>Delete this file
			    </button>
			{{ Form::close() }}

		</div>

		<div class="col-xs-4">

			<img src="{{ $photo->image->url('thumb') }}" class="img-responsive pull-right">

		</div>

	</div>

</div>