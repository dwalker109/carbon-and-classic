@extends('member.layout')

@section('title', 'Dashboard')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		@if (Confide::user()->is_profile_holder)

			<section id="dashboard-header">
				<div>
					<h1>Dashboard</h1>
				</div>
			</section>

			{{-- Buying --}}
			<section class="member-dashboard-boxout">

				<div class="member-dashboard-boxout-lhs">

					<h2>Buying</h2>
					
					<p><a href="{{ route('member.offers.payment-required') }}"
					class="member-dashboard-load-partial member-dashboard-load-partial-auto" 
					data-target-div="buying-target-div">Purchases awaiting payment</a></p>
				
					<p><a href="{{ route('member.offers.purchase-history') }}"
					class="member-dashboard-load-partial" 
					data-target-div="buying-target-div">Recent purchases</a></p>

					<p>{{ link_to_route('member.offers.purchases', 'View all my purchases') }}
						<span class="glyphicon glyphicon-new-window"> </span>
					</p>

				</div>

				<div class="member-dashboard-boxout-rhs" id="buying-target-div"></div>

			</section>

			{{-- Selling --}}
			<section class="member-dashboard-boxout">

				@if (! Confide::user()->is_extended_profile_holder)

				{{-- Not eligeable to sell, show error --}}
					<div class="member-dashboard-boxout-lhs">
						<h2>Selling</h2>
					</div>

					<div class="member-dashboard-boxout-rhs" id="selling-target-div">
						<p class="h2 text-warning text-center">You will not be able to sell until you 
						{{ link_to_route('member.profile.manage', 'add a PayPal ID to your user profile',
							null, ['class' => 'alert-link']) }}.</p>
					</div>

				@else

				{{-- Is extended profile holder, render data --}}
					<div class="member-dashboard-boxout-lhs">

							<h2>Selling</h2>


							<p><a href="{{ route('member.listings.all-active') }}"
							class="member-dashboard-load-partial member-dashboard-load-partial-auto" 
							data-target-div="selling-target-div">Currently selling</a></p>
							
							<p><a href="{{ route('member.listings.recently-sold') }}"
							class="member-dashboard-load-partial" 
							data-target-div="selling-target-div">Recently completed sales</a></p>

							<p>{{ link_to_route('member.listings.create', 'Add a new listing') }} 
								<span class="glyphicon glyphicon-new-window"> </span>
							</p>
							<p>{{ link_to_route('member.listings.index', 'View all my sales') }}
								<span class="glyphicon glyphicon-new-window"> </span>
							</p>

					</div>

					<div class="member-dashboard-boxout-rhs" id="selling-target-div"></div>
				@endif
			</section>

			{{-- Offers --}}
			<section class="member-dashboard-boxout">

				<div class="member-dashboard-boxout-lhs">

					<h2>Offers</h2>

					<p><a href="{{ route('member.offers.actioned') }}"
					class="member-dashboard-load-partial member-dashboard-load-partial-auto"
					data-target-div="offer-target-div">Accepted or declined by seller</a></p>

					<p><a href="{{ route('member.offers.awaiting_response') }}"
					class="member-dashboard-load-partial"
					data-target-div="offer-target-div">Awaiting a response from seller</a></p>

					<p>{{ link_to_route('member.offers.index', 'View all my offers') }}
						<span class="glyphicon glyphicon-new-window"> </span>
					</p>

				</div>

				<div class="member-dashboard-boxout-rhs" id="offer-target-div"></div>

			</section>

		@else

			<div class="jumbotron">
				<h1>Welcome!</h1>
				<p>Now that you have signed up you should create your profile.
				You will need to do this before you can submit a listing or complete
				a purchase.</p>
				<p>
					<a class="btn btn-primary btn-lg"
					href="{{ route('member.profile.manage') }}"
					role="button">Create my profile</a>
				</p>
			</div>

			<div class="jumbotron">
				<h2>Don't want to create a profile just yet?</h1>
				<p>No problem. If you just want to browse, 
				{{ link_to_route('buying_home', 'visit the buying homepage') }}
				and see what's available.</p>
			</div>

		@endif

	</div>

</div>

@stop
