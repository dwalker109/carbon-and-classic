@extends('member.layout')

@section('title', 'My profile')

@section('content')

<div class="row">

	<div class="col-md-12 col-lg-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h1>Profile</h1>
			</div>

			<div class="panel-body">

				<p>Complete these details in order to complete purchases &mdash;
				they will be passed to sellers automatically and will also be
				used to calculate postage costs.</p>
				<p class="text-info">If you intend to submit a listing of your own, you will
				need to complete your <a target="_blank"
				href="https://www.google.co.uk/?q=How+do+I+find+my+Paypal+ID">Paypal ID</a>
				beforehand. Please ensure it is correct or potential buyers will 
				not be able to complete their purchase!</p>

				{{ Former::open_for_files(route('member.profile.doManage'))->method('POST') }}

				{{ Former::populate($profile) }}
				{{ Former::withRules($rules) }}

				{{ Former::text('full_name') }}
				<span class="text-info">{{ Former::text('paypal_id', 'PayPal ID') }}</span>
				{{ Former::text('address_line_1') }}
				{{ Former::text('address_line_2') }}
				{{ Former::text('address_line_3') }}
				{{ Former::text('city') }}
				{{ Former::text('postal_code') }}
				{{ Former::select('telephone_country', 'Country')->options(Country::getList())->value('GB') }}
				{{ Former::text('telephone') }}
				{{ Former::file('avatar') }}

				{{-- Show current avatar section if exists --}}
				@if ($profile->avatar->size())
					<div class="form-group">
						<label class="control-label col-lg-2 col-sm-4" for="avatar">Current Avatar</label>
						<div class="col-lg-10 col-sm-8">
							<img src="{{ $profile->avatar->url('thumb') }}" class="pull-left thumbnail">
							<p class="text-muted text-right">You can upload a new avatar above, which will replace this one.</p>
						</div>
					</div>
				@endif

				{{ Former::actions(
					Button::primary('Save')->submit(),
					Button::danger('Cancel')->asLinkTo(route('member.dashboard'))
				) }}

				{{ Former::close() }}

			</div> {{-- /.panel-body --}}


		</div> {{-- /.panel-primary --}}
	</div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
