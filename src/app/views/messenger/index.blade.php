@extends('messenger.layout')

@section('title', 'Messages')

@section('content')

<div class="row">

    <div class="col-md-12 col-lg-12">

        <div class="panel panel-default">

            <div class="panel-heading">
                <h1>Messages</h1>
            </div>

            <div class="panel-body">

                @if($threads->count() > 0)

                    @foreach($threads as $thread)

                    <?php $class = $thread->isUnread(Confide::user()->id) ? 'alert-info' : '' ?>

                    <a href="{{ route('messages.show', ['id' => $thread->id]) }}">
                        <div class="media alert {{ $class }}">
                            <p class="pull-right">
                                Updated: {{ $thread->latest_message->updated_at->diffForHumans() }}
                            </p>
                            <h4 class="media-heading">
                                {{ $thread->subject }}
                            </h4>
                            <p><small><strong>Participants:</strong> 
                                {{ $thread->participantsString(null, ['username']) }}</small>
                            </p>
                        </div>
                    </a>
                    
                    @endforeach

                @else

                    <div class="well">
                        <h2>You don't have any messages at the moment</h2>
                        <p>To send a message, click the <b>Contact Seller</b>
                        link on any listing.</p>

                    </div>

                @endif

            </div> {{-- /.panel-body --}}
        </div> {{-- /.panel-primary --}}
    </div> {{-- /.col-* --}}
</div> {{-- /.row --}}

@stop
