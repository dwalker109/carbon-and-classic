{{-- Partial used as a bootstrop popover - nake sure to only use double quotes ("")
in this file, single quotes will break the calling htmk element!!! --}}

{{ Form::open(['route' => 'search']) }}

<div id="search-popover">
	<div class="input-group">
		<input type="text" name="freetext" class="form-control" placeholder="&hellip;">
	    <span class="input-group-btn">
			<input type="submit" class="btn btn-default" value="OK">
	    </span> {{-- ./input-class-btn --}}
	</div> {{-- ./input-group --}}
</div>

{{ Form::close() }}