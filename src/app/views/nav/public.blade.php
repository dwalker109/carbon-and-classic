<header class="navbar navbar-default cc-navbar-main">
	<div class="container-fluid">

		<div class="navbar-header">
			<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
			</button>
			<a href="/" class="navbar-brand">Carbon <b>&amp;</b> Classic</a>
		</div> {{-- ./navbar-header --}}

		<nav class="collapse navbar-collapse bs-navbar-collapse">

			<ul class="nav navbar-nav">

				<li>{{ link_to_route('buying_home', 'Buy') }}</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sell</a>
					<ul class="dropdown-menu" role="menu">
						<li>{{ link_to_route('member.listings.create', 'Add a new listing') }}</li>
						<li>{{ link_to_route('member.listings.index', 'View all my sales') }}</li>
					</ul>
				</li>

				<li>{{ link_to_route('page', 'About', ['slug' => 'about-us']) }}</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Contact</a>
					<ul class="dropdown-menu" role="menu">
						<li>{{ link_to_route('page', 'Support', ['slug' => 'support']) }}</li>
						<li>{{ link_to_route('page', 'FAQ', ['slug' => 'faq']) }}</li>
					</ul>
				</li>

			</ul>

			<ul class="nav navbar-nav navbar-right">

				@if (! Confide::user())
					<li>{{ link_to_route('users.login_and_return', 'Sign in') }}</li>
				@endif

				{{-- Search form (single quotes on the data-content attrib are crucial) --}}
				<li><a class="show-popover" role="button" 
				data-content='@include('nav.search-form')'>Search</a></li>

				<li><a href="#" class="social-link"><img src="/images/facebook.png"> <span>Facebook</span></a></li>
				<li><a href="#" class="social-link"><img src="/images/twitter.png"> <span>Twitter</span></a></li>
				<li><a href="#" class="social-link"><img src="/images/instagram.png"> <span>Instragram</span></a></li>
				<li><a href="#" class="social-link"><img src="/images/pinterest.png"> <span>Pinterest</span></a></li>

			</ul>

		</nav>

	</div> {{-- ./container --}}
</header>
