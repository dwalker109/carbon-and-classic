@extends('public.layout')

@section('content')

<section class="auth-main">

    <div class="auth-form-container">

        <h2>Almost there...</h2>
        <p>Your account has been created and you will soon receive an email to 
        confirm your account. Just follow the instructions in the email and you
        will be able to login straight away.</p>

    </div>

</section>

@stop