/**
 * Bind jQuery functionality to page elements
 */

$(document).ready(function() {

	// jQuery DataTables
	$.extend( $.fn.dataTable.defaults, {
	    ordering:  true,
	    stateSave: true
	} );
	$( '.data-table' ).DataTable();

	$.extend( $.fn.dataTable.defaults, {
	    ordering:  false,
	    search: false,
	    stateSave: false
	} );
	$( '.data-table-plain' ).DataTable();
	
	// Alerts Bootbox popups (with mark as read)
	$(document).on("click", ".bootbox-alerts-popup", function(e) {
		e.preventDefault();
		$.get($(this).data("read-url"));
		bootbox.alert($(this).data("message"));
	});

	// Bootstrap Filestyle
	$(':file').filestyle();

	// Init HTML5 Sortable
	$('.sortable').sortable({
		forcePlaceholderSize: true
	});

	// Load dashboard partials in current page via AJAX
	$('a.member-dashboard-load-partial').click(function(e) {
		e.preventDefault();
		
		$(this).parent().siblings().removeClass('active-item');
		$(this).parent().addClass('active-item');

		var target_div_selector = "#" + $(this).data('target-div');
		$(target_div_selector).html('<div class="text-center"><img src="/images/squares.gif"></div>');
		$(target_div_selector).load($(this).attr('href'));
	});
	// Auto click these defaults on page load
	if ($('a.member-dashboard-load-partial-auto').length > 0) {
		$('a.member-dashboard-load-partial-auto').click();
	}
});
