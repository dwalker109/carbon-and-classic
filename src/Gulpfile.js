// Get modules
var gulp = require('gulp');
var less = require('gulp-less');
var notify = require('gulp-notify');


// Task Less
gulp.task('less', function () {

	// Compile master files (not _all_ less files in subdirs)
    gulp.src('./app/less/master/*.less')
    
    // Handle errors - fugly!
    .pipe(
    	less(function () {
			this.emit("error", new Error('Error!'));
	    })
    )
    .on("error", notify.onError(function (error) {
        return {
        	title: "LESS compile failed",
        	message: error.message
        };
    }))
    
    // Pipe to destination
    .pipe(gulp.dest('./public/styles/compiled'))
    
    // Display success message
	.pipe(
		notify({
			title: "LESS compile succeeded", 
			message: "<%= file.relative %>"
		})
	);
});


// Watch _all_ less files
gulp.task('watch', function() {
	gulp.watch('./app/less/**/*.less', ['less']);

});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch']);
